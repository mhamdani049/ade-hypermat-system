@extends('layouts.main')
@section('content')

    <section class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
         		 <div class="col-sm-6">
            		<h1>Pencairan</h1>
          		</div>
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              			<li class="breadcrumb-item"><a href="{{ url('/pencairanDana') }}">Pencairan</a></li>
			  			<li class="breadcrumb-item active">Edit</li>
            		</ol>
          		</div>
        	</div>
      	</div>
    </section>

    <section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Form</h3>
			</div>
			<form class="form-horizontal" action="{{ url('/pencairanDana/doUpdate')}}" method="post">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group row">
					<label for="tanggal_pdk" class="col-sm-2 col-form-label">Tanggal PDK</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="tanggal_pdk" name="tanggal_pdk" value="{{ $data->tanggal_pdk }}" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="nominal_pdk" class="col-sm-2 col-form-label">Nominal PDK</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nominal_pdk" name="nominal_pdk" value="{{ $data->nominal_pdk }}" required>
					</div>
				</div>
				<!-- <div class="form-group row">
					<label for="nominal_pdk" class="col-sm-2 col-form-label">Akun</label>
					<div class="col-sm-10">
						<select class="form-control" id="id_akun" name="id_akun" required>
                          <option value="">-- Choose --</option>
						  @foreach ($akun as $ak)
						  	<option value="{{$ak->id_akun}}" {{($ak->id_akun == $data->id_akun) ? 'selected' : ''}}>{{$ak->nama_akun}}</option> 
						  @endforeach
                        </select>
					</div>
				</div> -->
			</div>
				<div class="card-footer">
				<input type="hidden" name="id_pencairan" value="{{ $data->id_pencairan }}">
					<button type="submit" class="btn btn-warning">Update</button>
					<a class="btn btn-default float-right" href="{{ url('/pencairanDana') }}">Batal</a>
				</div>
			</form>
			</div>
		</div>
    </section>

@endsection
