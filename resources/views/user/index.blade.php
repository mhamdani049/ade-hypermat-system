@extends('layouts.main')
@section('content')

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              <li class="breadcrumb-item active">User</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
		  <div class="card-header">
		  	<a class="btn btn-success" href="{{ url('/user/add') }}">Add New</a>
          </div>
          <div class="card-body">
		  	@if(\Session::has('alert'))
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fas fa-ban"></i> Error</h5>
				{{Session::get('alert')}}
			</div>
			@endif
			@if(\Session::has('alert-success'))
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fas fa-ban"></i> Success</h5>
				{{Session::get('alert-success')}}
			</div>
			@endif
		  	<table class="table table-bordered" id="dt-table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Akses</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data as $row)
					<tr>
						<td>{{$row->id}}</td>
						<td>{{$row->name}}</td>
						<td>{{$row->name_akses}}</td>
						<td>
							<a id="edit" class="btn btn-warning btn-sm" href="user/edit/{{$row->id}}/{{$row->id_akses}}">Edit</a> 
							<a id="delete" class="btn btn-danger btn-sm" href="user/delete/{{$row->id}}/{{$row->id_akses}}">Delete</a> 
							<a id="changePassword" class="btn btn-info btn-sm" href="user/changePassword/{{$row->id}}/{{$row->id_akses}}">Change Password</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
          </div>
        </div>
      </div>
    </section>

@endsection

