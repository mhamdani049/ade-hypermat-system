@extends('layouts.main')
@section('content')

    <section class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
         		 <div class="col-sm-6">
            		<h1>User</h1>
          		</div>
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              			<li class="breadcrumb-item"><a href="{{ url('/user') }}">User</a></li>
			  			<li class="breadcrumb-item active">Change Password</li>
            		</ol>
          		</div>
        	</div>
      	</div>
    </section>

    <section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Form</h3>
			</div>
			<form class="form-horizontal" action="{{ url('/user/doChangePassword')}}" method="post">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group row">
					<label for="password" class="col-sm-2 col-form-label">New Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="password" name="password" required>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<input type="hidden" name="id" value="{{ $data->id }}">
				<input type="hidden" name="_id_akses" value="{{ $data->id_akses }}">
				<button type="submit" class="btn btn-warning">Change Password</button>
				<a class="btn btn-default float-right" href="{{ url('/user') }}">Batal</a>
			</div>
			</form>
			</div>
		</div>
    </section>

@endsection
