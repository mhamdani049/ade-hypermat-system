@extends('layouts.main')
@section('content')

    <section class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
         		 <div class="col-sm-6">
            		<h1>User</h1>
          		</div>
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              			<li class="breadcrumb-item"><a href="{{ url('/user') }}">User</a></li>
			  			<li class="breadcrumb-item active">Edit</li>
            		</ol>
          		</div>
        	</div>
      	</div>
    </section>

    <section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Form</h3>
			</div>
			<form class="form-horizontal" action="{{ url('/user/doUpdate')}}" method="post">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group row">
					<label for="id" class="col-sm-2 col-form-label">ID</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="id" name="id" value="{{ $data->id }}" disabled required>
					</div>
				</div>
				<div class="form-group row">
					<label for="name" class="col-sm-2 col-form-label">Name</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="name" name="name" value="{{ $data->name }}" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="id_akses" class="col-sm-2 col-form-label">Hak Akses</label>
					<div class="col-sm-10">
						<select class="form-control" id="id_akses" name="id_akses" disabled required>
                          <option value="">-- Choose --</option>
						  @foreach ($hasAkses as $row)
							<option value="{{$row->id_akses}}" {{($row->id_akses == $data->id_akses) ? 'selected' : ''}}>{{$row->nama}}</option> 
						  @endforeach
                        </select>
					</div>
				</div>
				<div class="form-group row">
					<label for="id_akses" class="col-sm-2 col-form-label">Jabatan</label>
					<div class="col-sm-10">
						<select class="form-control" id="jabatan" name="jabatan" required>
                          <option value="">-- Choose --</option>
						  @foreach ($jabatan as $row)
						  	<option value="{{$row}}" {{($row == $data->jabatan) ? 'selected' : ''}}>{{$row}}</option> 
						  @endforeach
                        </select>
					</div>
				</div>
			</div>
				<div class="card-footer">
					<input type="hidden" name="id" value="{{ $data->id }}">
					<input type="hidden" name="_id_akses" value="{{ $data->id_akses }}">
					<button type="submit" class="btn btn-warning">Update</button>
					<a class="btn btn-default float-right" href="{{ url('/user') }}">Batal</a>
				</div>
			</form>
			</div>
		</div>
    </section>

@endsection
