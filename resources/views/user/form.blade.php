@extends('layouts.main')
@section('content')

    <section class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
         		 <div class="col-sm-6">
            		<h1>User</h1>
          		</div>
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              			<li class="breadcrumb-item"><a href="{{ url('/user') }}">User</a></li>
			  			<li class="breadcrumb-item active">Add New</li>
            		</ol>
          		</div>
        	</div>
      	</div>
    </section>

    <section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Form</h3>
			</div>
			<form class="form-horizontal" action="{{ url('/user/doAdd')}}" method="post">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group row">
					<label for="id" class="col-sm-2 col-form-label">ID</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="id" name="id" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="name" class="col-sm-2 col-form-label">Name</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="name" name="name" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="id_akses" class="col-sm-2 col-form-label">Hak Akses</label>
					<div class="col-sm-10">
						<select class="form-control" id="id_akses" name="id_akses" required>
                          <option value="">-- Choose --</option>
						  @foreach ($hasAkses as $row)
						  	<option value="{{$row->id_akses}}">{{$row->nama}}</option> 
						  @endforeach
                        </select>
					</div>
				</div>
				<div class="form-group row">
					<label for="id_akses" class="col-sm-2 col-form-label">Jabatan</label>
					<div class="col-sm-10">
						<select class="form-control" id="jabatan" name="jabatan" required>
                          <option value="">-- Choose --</option>
						  @foreach ($jabatan as $row)
						  	<option value="{{$row}}">{{$row}}</option> 
						  @endforeach
                        </select>
					</div>
				</div>
				<hr />
				<div class="form-group row">
					<label for="password" class="col-sm-2 col-form-label">Password</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="password" name="password" required>
					</div>
				</div>
			</div>
				<div class="card-footer">
					<button type="submit" class="btn btn-info">Simpan</button>
					<a class="btn btn-default float-right" href="{{ url('/user') }}">Batal</a>
				</div>
			</form>
			</div>
		</div>
    </section>

@endsection
