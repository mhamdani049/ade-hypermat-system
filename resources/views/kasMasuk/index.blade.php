@extends('layouts.main')
@section('content')

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Kas Masuk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              <li class="breadcrumb-item active">Kas Masuk</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
          <div class="card-body">
		  	@if(\Session::has('alert'))
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fas fa-ban"></i> Error</h5>
				{{Session::get('alert')}}
			</div>
			@endif
			@if(\Session::has('alert-success'))
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fas fa-ban"></i> Success</h5>
				{{Session::get('alert-success')}}
			</div>
			@endif
		  	<table class="table table-bordered" id="dt-table">
				<thead>
					<tr>
						<th>Id</th>
						<th>Tanggal PDK</th>
						<th>Nominal PDK</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
          </div>
        </div>
      </div>
    </section>

	<script type="text/javascript">
		var id_akses = "{{Session::get('user')['id_akses']}}";

		$(function() {
			var dataTable = $('#dt-table').DataTable({
				processing: true,
				serverSide: true,
				order: [[ 3, "asc" ]],
				ajax: 'kasMasuk/json',
				columns: [
					{ data: 'id_pencairan', name: 'id_pencairan' },
					{ data: 'tanggal_pdk', name: 'tanggal_pdk' },
					{ data: 'nominal_pdk', name: 'nominal_pdk' },
					{ data: 'status', name: 'status' }
				],
				"columnDefs": [{
					"targets": 4,
					"data": null,
					"render": function(data, type, row, meta) {
						var button = '';
						if (row.status === 0) button = '<button id="edit" class="btn btn-warning btn-sm">Edit</button>';
						return button;
					}
				},{ 
					"targets": 3,
					"render": function(data, type, row, meta) {

						var status = '<p class="text-primary">Pending</p>';
						if (data === 1) status = '<p class="text-success">Approved</p>';
						else if (data === 2) status = '<p class="text-danger">Rejected</p>';
						return status;  
					}
				},{ 
					"targets": 2,
					"render": function(data, type, row, meta) {
						return numberComma(data); 
					}
				}]
			});

			$('#dt-table tbody').on('click', '#edit', function () {
				var data = dataTable.row( $(this).parents('tr') ).data();
				window.location.href = "kasMasuk/edit/"+ data.id_pencairan;
			});
		});

		function numberComma(num) {
			return Math.round(num).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
		}
    
	</script>

@endsection
