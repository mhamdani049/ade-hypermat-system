@extends('layouts.main')
@section('content')

	<div style="display: none">
		{{ $grandTotalDebitBPromosi = 0 }}
		{{ $grandTotalDebitBSewa = 0 }}
		{{ $grandTotalDebitBTolParkir = 0 }}
		{{ $grandTotalDebitPembelianBarang = 0 }}
		{{ $grandTotalDebitBiayaLainlain = 0 }}
		{{ $grandTotalKreditKasKecil = 0 }}
	</div>

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Jurnal Kas Keluar</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              <li class="breadcrumb-item active">Jurnal Kas Keluar</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
		  <div class="card-header">
			<div class="row">
				<div class="col-md-4">
					<form class="form-horizontal" action="{{ url('/kasKeluar/doSearchJurnalKasKeluar')}}" method="post">
						{{ csrf_field() }}
						<div class="form-group row">
							<label for="start" class="col-sm-2 col-form-label">Periode</label>
							<div class="col-sm-10">
								<input type="date" class="form-control" id="start" name="start" value="{{ $start }}" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="end" class="col-sm-2 col-form-label">&nbsp;</label>
							<div class="col-sm-10">
								<input type="date" class="form-control" id="end" name="end" value="{{ $end }}" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="end" class="col-sm-2 col-form-label">&nbsp;</label>
							<div class="col-sm-10">
								<button type="submit" class="btn btn-info">Search</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<p>Search : {{$start}} - {{$end}}</p>
          </div>
          <div class="card-body">
		  	<table class="table table-bordered">
				<tr>
					<th rowspan="2">Tanggal</th>
					<th rowspan="2">Keterangan</th>
					<th colspan="5">Debit</th>
					<th>Kredit</th>
				</tr>
				<tr>
					<th>B. Promosi</th>
					<th>B. Sewa</th>
					<th>B. Tol & Parkir</th>
					<th>Pembelian Barang</th>
					<th>Biaya Lain-lain</th>
					<th>Kas Kecil</th>
				</tr>  
				@forelse($datas as $row)
					<tr>
						<td>{{$row->tanggal}}</td>
						<td>{{$row->keterangan}}</td>
						<td>
							@if($row->nama_akun == 'Biaya Promosi')
								@php
									echo App\Http\Controllers\KasMasukController::rp($row->nominal);
								@endphp
								<div style="display: none">{{$grandTotalDebitBPromosi += $row->nominal}}</div>
							@endif
						</td>
						<td>
							@if($row->nama_akun == 'Biaya Sewa')
								@php
									echo App\Http\Controllers\KasMasukController::rp($row->nominal);
								@endphp
								<div style="display: none">{{$grandTotalDebitBSewa += $row->nominal}}</div>
							@endif
						</td>
						<td>
							@if($row->nama_akun == 'Biaya TOL, Parkir, Bensin')
								@php
									echo App\Http\Controllers\KasMasukController::rp($row->nominal);
								@endphp
								<div style="display: none">{{$grandTotalDebitBTolParkir += $row->nominal}}</div>
							@endif
						</td>
						<td>
							@if($row->nama_akun == 'Pembelian Barang')
								@php
									echo App\Http\Controllers\KasMasukController::rp($row->nominal);
								@endphp
								<div style="display: none">{{$grandTotalDebitPembelianBarang += $row->nominal}}</div>
							@endif
						</td>
						<td>
							@if($row->nama_akun == 'Biaya Lain-lain')
								@php
									echo App\Http\Controllers\KasMasukController::rp($row->nominal);
								@endphp
								<div style="display: none">{{$grandTotalDebitBiayaLainlain += $row->nominal}}</div>
							@endif
						</td>
						<td>
							@php
								echo App\Http\Controllers\KasMasukController::rp($row->nominal);
							@endphp
							<div style="display: none">{{$grandTotalKreditKasKecil += $row->nominal}}</div>
						</td>
					</tr>
				@empty
					<tr>
						<td colspan="100">No data.</td>
					</tr>
				@endforelse
				<tr>
					<th colspan="2">Total</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitBPromosi);
						@endphp
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitBSewa);
						@endphp
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitBTolParkir);
						@endphp
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitPembelianBarang);
						@endphp
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitBiayaLainlain);
						@endphp
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalKreditKasKecil);
						@endphp
					</th>
				</tr>  
			</table>
			<br /><br />
			<p>
				<a class="btn btn-primary btn-lg" href="print/download/{{$s}}/{{$e}}">Cetak</a>
			</p>
          </div>
        </div>
      </div>
    </section>

@endsection

