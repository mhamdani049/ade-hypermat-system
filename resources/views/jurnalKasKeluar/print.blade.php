<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		body {font-size: 6; width: 100%;}
		table {border-collapse: collapse;}
		td    {padding: 0px;}
	</style>
	<div style="display: none">
		{{ $grandTotalDebitBPromosi = 0 }}
		{{ $grandTotalDebitBSewa = 0 }}
		{{ $grandTotalDebitBTolParkir = 0 }}
		{{ $grandTotalDebitPembelianBarang = 0 }}
		{{ $grandTotalDebitBiayaLainlain = 0 }}
		{{ $grandTotalKreditKasKecil = 0 }}
	</div>
	<table class="table table-bordered" width="100%">
		<tr>
			<td>
				<img src="{!!asset('img/hypermart-1.png')!!}" width="180" />
			</td>
			<td colspan="8">
				<h7>Jurnal Kas Keluar</h7>
				<p>Hypermart Sentul ({{$s}} s/d {{$e}})</p>
			</td>
		</tr>
		<tr>
			<th rowspan="2">Tanggal</th>
			<th rowspan="2">Keterangan</th>
			<th colspan="5">Debit</th>
			<th>Kredit</th>
		</tr>
		<tr>
			<th>B. Promosi</th>
			<th>B. Sewa</th>
			<th>B. Tol & Parkir</th>
			<th>Pembelian Barang</th>
			<th>Biaya Lain-lain</th>
			<th>Kas Kecil</th>
		</tr>  
		@forelse($data as $row)
			<tr>
				<td>{{$row->tanggal}}</td>
				<td>{{$row->keterangan}}</td>
				<td>
					@if($row->nama_akun == 'Biaya Promosi')
						@php
							echo App\Http\Controllers\KasMasukController::rp($row->nominal);
						@endphp
						<div style="display: none">{{$grandTotalDebitBPromosi += $row->nominal}}</div>
					@endif
				</td>
				<td>
					@if($row->nama_akun == 'Biaya Sewa')
						@php
							echo App\Http\Controllers\KasMasukController::rp($row->nominal);
						@endphp
						<div style="display: none">{{$grandTotalDebitBSewa += $row->nominal}}</div>
					@endif
				</td>
				<td>
					@if($row->nama_akun == 'Biaya TOL, Parkir, Bensin')
						@php
							echo App\Http\Controllers\KasMasukController::rp($row->nominal);
						@endphp
						<div style="display: none">{{$grandTotalDebitBTolParkir += $row->nominal}}</div>
					@endif
				</td>
				<td>
					@if($row->nama_akun == 'Pembelian Barang')
						@php
							echo App\Http\Controllers\KasMasukController::rp($row->nominal);
						@endphp
						<div style="display: none">{{$grandTotalDebitPembelianBarang += $row->nominal}}</div>
					@endif
				</td>
				<td>
					@if($row->nama_akun == 'Biaya Lain-lain')
						@php
							echo App\Http\Controllers\KasMasukController::rp($row->nominal);
						@endphp
						<div style="display: none">{{$grandTotalDebitBiayaLainlain += $row->nominal}}</div>
					@endif
				</td>
				<td>
					@php
						echo App\Http\Controllers\KasMasukController::rp($row->nominal);
					@endphp
					<div style="display: none">{{$grandTotalKreditKasKecil += $row->nominal}}</div>
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="100">No data.</td>
			</tr>
		@endforelse
		<tr>
			<th colspan="2">Total</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitBPromosi);
				@endphp
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitBSewa);
				@endphp
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitBTolParkir);
				@endphp
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitPembelianBarang);
				@endphp
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitBiayaLainlain);
				@endphp
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalKreditKasKecil);
				@endphp
			</th>
		</tr>  
	</table>
</body>
</html>