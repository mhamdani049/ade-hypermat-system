@extends('layouts.main')
@section('content')

    <section class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
         		 <div class="col-sm-6">
            		<h1>Kas Keluar</h1>
          		</div>
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              			<li class="breadcrumb-item"><a href="{{ url('/kasKeluar') }}">Kas Keluar</a></li>
			  			<li class="breadcrumb-item active">Edit</li>
            		</ol>
          		</div>
        	</div>
      	</div>
    </section>

    <section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Form</h3>
			</div>
			<form class="form-horizontal" action="{{ url('/kasKeluar/doUpdate')}}" method="post">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group row">
					<label for="tanggal_pjd" class="col-sm-2 col-form-label">Tanggal PJD</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="tanggal_pjd" value="{{ $data->tanggal_pjd }}" disabled required>
					</div>
				</div>
				<div class="form-group row">
					<label for="nominal" class="col-sm-2 col-form-label">Nominal PJD</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nominal" value="{{ $data->nominal_pjd }}" disabled required>
					</div>
				</div>
				<hr />
				<div class="form-group row">
					<label for="tanggal" class="col-sm-2 col-form-label">Tanggal Kas Keluar*</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $data->tanggal }}" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="id_akun" class="col-sm-2 col-form-label">Akun*</label>
					<div class="col-sm-10">
						<select class="form-control" id="id_akun" name="id_akun" required>
                          <option value="">-- Choose --</option>
						  @foreach ($akun as $ak)
						  	<option value="{{$ak->id_akun}}" {{($ak->id_akun == $data->id_akun) ? 'selected' : ''}}>{{$ak->nama_akun}}</option> 
						  @endforeach
                        </select>
					</div>
				</div>
				<div class="form-group row">
					<label for="status" class="col-sm-2 col-form-label">Status*</label>
					<div class="col-sm-10">
						<select class="form-control" id="status" name="status" required>
                          <option value="">-- Choose --</option>
						  <option value="1">Approved</option>
						  <option value="2">Rejected</option>
                        </select>
					</div>
				</div>
				<div class="form-group row">
					<label for="reason" class="col-sm-2 col-form-label">Keterangan*</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="reason" name="keterangan" rows="5" required></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label for="reason" class="col-sm-2 col-form-label">Alasan</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="reason" name="reason" rows="3"></textarea>
						<small>Alsan diisi apabila Anda memilih <b>status Rejected</b>.</small>
					</div>
				</div>
			</div>
				<div class="card-footer">
					<input type="hidden" name="id_pengajuan" value="{{ $data->id_pengajuan }}">
					<input type="hidden" name="nominal" value="{{ $data->nominal_pjd }}">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a class="btn btn-default float-right" href="{{ url('/kasKeluar') }}">Batal</a>
				</div>
			</form>
			</div>
		</div>
    </section>

@endsection
