<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		body {font-size: 9;}
		table {border-collapse: collapse;}
		td    {padding: 6px;}
	</style>
	<div style="display: none">
		{{ $grandTotalDebitKasKecil = 0 }}
		{{ $grandTotalDebitPKas = 0 }}
		{{ $grandTotalKreditKasBank = 0 }}
		{{ $grandTotalKreditLainlain = 0 }}
	</div>
	<table class="table table-bordered" width="100%">
		<tr>
			<td>
				<img src="{!!asset('img/hypermart-1.png')!!}" width="180" />
			</td>
			<td colspan="5">
				<h7>Jurnal Kas Masuk</h7>
				<p>Hypermart Sentul ({{$s}} s/d {{$e}})</p>
			</td>
		</tr>
		<tr>
			<th rowspan="2">Tanggal</th>
			<th rowspan="2">Keterangan</th>
			<th colspan="2">Debit</th>
			<th colspan="2">Kredit</th>
		</tr>
		<tr>
			<th>Kas Kecil</th>
			<th>P. Kas</th>
			<th>Kas Bank</th>
			<th>Other Kas</th>
		</tr>  
		@forelse($data as $row)
			<tr>
				<td>{{$row->tanggal}}</td>
				<td>{{$row->keterangan}}</td>
				<td>
					@if($row->nama_akun == 'Kas Kecil' || $row->nama_akun == 'Kas Bank')
						@php
							echo App\Http\Controllers\KasMasukController::rp($row->nominal);
						@endphp
						<div style="display: none">{{$grandTotalDebitKasKecil += $row->nominal}}</div>
					@endif
				</td>
				<td>
					@if($row->nama_akun == 'Pengembalian Kas' || $row->nama_akun == 'Other Kas')
						@php
							echo App\Http\Controllers\KasMasukController::rp($row->nominal);
						@endphp
						<div style="display: none">{{$grandTotalDebitPKas += $row->nominal}}</div>
					@endif
				</td>
				<td>
					@if($row->nama_akun == 'Kas Kecil' || $row->nama_akun == 'Kas Bank')
						@php
							echo App\Http\Controllers\KasMasukController::rp($row->nominal);
						@endphp
						<div style="display: none">{{$grandTotalKreditKasBank += $row->nominal}}</div>
					@endif
				</td>
				<td>
					@if($row->nama_akun == 'Pengembalian Kas' || $row->nama_akun == 'Other Kas')
						@php
							echo App\Http\Controllers\KasMasukController::rp($row->nominal);
						@endphp
						<div style="display: none">{{$grandTotalKreditLainlain += $row->nominal}}</div>
					@endif
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="100">No data.</td>
			</tr>
		@endforelse
		<tr>
			<th colspan="2">Total</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitKasKecil);
				@endphp
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitPKas);
				@endphp
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalKreditKasBank);
				@endphp
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalKreditLainlain);
				@endphp
			</th>
		</tr>  
	</table>
</body>
</html>