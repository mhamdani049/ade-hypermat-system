@extends('layouts.main')
@section('content')

	<div style="display: none">
		{{ $grandTotalDebitKasKecil = 0 }}
		{{ $grandTotalDebitPKas = 0 }}
		{{ $grandTotalKreditKasBank = 0 }}
		{{ $grandTotalKreditLainlain = 0 }}
	</div>

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Jurnal Kas Masuk</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              <li class="breadcrumb-item active">Jurnal Kas Masuk</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
		  <div class="card-header">
			<div class="row">
				<div class="col-md-4">
					<form class="form-horizontal" action="{{ url('/kasMasuk/doSearchJurnalKasMasuk')}}" method="post">
						{{ csrf_field() }}
						<div class="form-group row">
							<label for="start" class="col-sm-2 col-form-label">Periode</label>
							<div class="col-sm-10">
								<input type="date" class="form-control" id="start" name="start" value="{{ $start }}" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="end" class="col-sm-2 col-form-label">&nbsp;</label>
							<div class="col-sm-10">
								<input type="date" class="form-control" id="end" name="end" value="{{ $end }}" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="end" class="col-sm-2 col-form-label">&nbsp;</label>
							<div class="col-sm-10">
								<button type="submit" class="btn btn-info">Search</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<p>Search : {{$start}} - {{$end}}</p>
          </div>
          <div class="card-body">
		  	<table class="table table-bordered">
				<tr>
					<th rowspan="2">Tanggal</th>
					<th rowspan="2">Keterangan</th>
					<th colspan="2">Debit</th>
					<th colspan="2">Kredit</th>
				</tr>
				<tr>
					<th>Kas Kecil</th>
					<th>P. Kas</th>
					<th>Kas Bank</th>
					<th>Other Kas</th>
				</tr>  
				@forelse($datas as $row)
					<tr>
						<td>{{$row->tanggal}}</td>
						<td>{{$row->keterangan}}</td>
						<td>
							@if($row->nama_akun == 'Kas Kecil' || $row->nama_akun == 'Kas Bank')
								@php
									echo App\Http\Controllers\KasMasukController::rp($row->nominal);
								@endphp
								<div style="display: none">{{$grandTotalDebitKasKecil += $row->nominal}}</div>
							@endif
						</td>
						<td>
							@if($row->nama_akun == 'Pengembalian Kas' || $row->nama_akun == 'Other Kas')
								@php
									echo App\Http\Controllers\KasMasukController::rp($row->nominal);
								@endphp
								<div style="display: none">{{$grandTotalDebitPKas += $row->nominal}}</div>
							@endif
						</td>
						<td>
							@if($row->nama_akun == 'Kas Kecil' || $row->nama_akun == 'Kas Bank')
								@php
									echo App\Http\Controllers\KasMasukController::rp($row->nominal);
								@endphp
								<div style="display: none">{{$grandTotalKreditKasBank += $row->nominal}}</div>
							@endif
						</td>
						<td>
							@if($row->nama_akun == 'Pengembalian Kas' || $row->nama_akun == 'Other Kas')
								@php
									echo App\Http\Controllers\KasMasukController::rp($row->nominal);
								@endphp
								<div style="display: none">{{$grandTotalKreditLainlain += $row->nominal}}</div>
							@endif
						</td>
					</tr>
				@empty
					<tr>
						<td colspan="100">No data.</td>
					</tr>
				@endforelse
				<tr>
					<th colspan="2">Total</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitKasKecil);
						@endphp
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalDebitPKas);
						@endphp
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalKreditKasBank);
						@endphp
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalKreditLainlain);
						@endphp
					</th>
				</tr>  
			</table>
			<br /><br />
			<p>
				<a class="btn btn-primary btn-lg" href="print/download/{{$s}}/{{$e}}">Cetak</a>
			</p>
          </div>
        </div>
      </div>
    </section>

@endsection

