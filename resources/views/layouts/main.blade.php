<!DOCTYPE html>
<html>
<head>
	@include('layouts.partials.head')

	<!-- jQuery -->
	<script src="{!!asset('AdminLTE-3.0.1/plugins/jquery/jquery.min.js')!!}"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="{!!asset('AdminLTE-3.0.1/plugins/jquery-ui/jquery-ui.min.js')!!}"></script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  @include('layouts.partials.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
  	@include('layouts.partials.nav')
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	@yield('content')
  </div>
  <!-- /.content-wrapper -->

  <!-- @include('layouts.partials.footer') -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{!!asset('AdminLTE-3.0.1/plugins/bootstrap/js/bootstrap.bundle.min.js')!!}"></script>
<!-- ChartJS -->
<script src="{!!asset('AdminLTE-3.0.1/plugins/chart.js/Chart.min.js')!!}"></script>
<!-- Sparkline -->
<script src="{!!asset('AdminLTE-3.0.1/plugins/sparklines/sparkline.js')!!}"></script>
<!-- JQVMap -->
<!-- <script src="{!!asset('AdminLTE-3.0.1/plugins/jqvmap/jquery.vmap.min.js')!!}"></script>
<script src="{!!asset('AdminLTE-3.0.1/plugins/jqvmap/maps/jquery.vmap.usa.js')!!}"></script> -->
<!-- jQuery Knob Chart -->
<script src="{!!asset('AdminLTE-3.0.1/plugins/jquery-knob/jquery.knob.min.js')!!}"></script>
<!-- daterangepicker -->
<script src="{!!asset('AdminLTE-3.0.1/plugins/moment/moment.min.js')!!}"></script>
<script src="{!!asset('AdminLTE-3.0.1/plugins/daterangepicker/daterangepicker.js')!!}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{!!asset('AdminLTE-3.0.1/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')!!}"></script>
<!-- Summernote -->
<script src="{!!asset('AdminLTE-3.0.1/plugins/summernote/summernote-bs4.min.js')!!}"></script>
<!-- DataTables -->
<script src="{!!asset('AdminLTE-3.0.1/plugins/datatables/jquery.dataTables.js')!!}"></script>
<script src="{!!asset('AdminLTE-3.0.1/plugins/datatables-bs4/js/dataTables.bootstrap4.js')!!}"></script>
<!-- overlayScrollbars -->
<script src="{!!asset('AdminLTE-3.0.1/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')!!}"></script>
<!-- AdminLTE App -->
<script src="{!!asset('AdminLTE-3.0.1/dist/js/adminlte.js')!!}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="{!!asset('AdminLTE-3.0.1/dist/js/pages/dashboard.js')!!}"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="{!!asset('AdminLTE-3.0.1/dist/js/demo.js')!!}"></script>
</body>
</html>
