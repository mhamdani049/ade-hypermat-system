<nav class="main-header navbar navbar-expand navbar-white navbar-light">

<!-- Right navbar links -->
<ul class="navbar-nav ml-auto">
	<li class="nav-item">
	<a class="nav-link" href="{{ url('/logout') }}">
		<i class="fas fa-th-large"></i>
	</a>
	</li>
</ul>
</nav>