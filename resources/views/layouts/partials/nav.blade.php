<!-- Brand Logo -->
<a href="index3.html" class="brand-link">
	<img src="{!!asset('AdminLTE-3.0.1/dist/img/AdminLTELogo.png')!!}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
		style="opacity: .8">
	<span class="brand-text font-weight-light">Hypermart SYSTEM</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
	<!-- Sidebar user panel (optional) -->
	<div class="user-panel mt-3 pb-3 mb-3 d-flex">
	<div class="image">
		<img src="{!!asset('AdminLTE-3.0.1/dist/img/user2-160x160.jpg')!!}" class="img-circle elevation-2" alt="User Image">
	</div>
	<div class="info">
		<a href="#" class="d-block">{{ Session::get('user')['name'] }}</a>
	</div>
	</div>

	<!-- Sidebar Menu -->
	<nav class="mt-2">
	<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
		<!-- Add icons to the links using the .nav-icon class
			with font-awesome or any other icon font library -->
		<li class="nav-item">
		<a href="{{ url('/') }}" class="nav-link">
			<i class="nav-icon fas fa-th"></i>
			<p>
			Home
			<span class="right badge badge-danger">Main</span>
			</p>
		</a>
		</li>
		@if (Session::get('user')['id_akses'] == 2 || Session::get('user')['id_akses'] == 4 || Session::get('user')['id_akses'] == 1)
		<li class="nav-item">
		<a href="{{ url('/pencairanDana') }}" class="nav-link">
			<i class="nav-icon fas fa-th"></i>
			<p>
			Pencarian
			</p>
		</a>
		</li>
		@endif
		@if (Session::get('user')['id_akses'] == 3 || Session::get('user')['id_akses'] == 4 || Session::get('user')['id_akses'] == 1)
		<li class="nav-item">
		<a href="{{ url('/pengajuanDana') }}" class="nav-link">
			<i class="nav-icon fas fa-th"></i>
			<p>
			Pengajuan
			</p>
		</a>
		</li>
		@endif
		@if (Session::get('user')['id_akses'] == 4 || Session::get('user')['id_akses'] == 1)
		<li class="nav-item has-treeview">
		<a href="#" class="nav-link">
			<i class="nav-icon fas fa-table"></i>
			<p>
			Master
			<i class="fas fa-angle-left right"></i>
			</p>
		</a>
		<ul class="nav nav-treeview">
			<li class="nav-item">
			<a href="{{ url('/akun') }}" class="nav-link">
				<i class="far fa-circle nav-icon"></i>
				<p>Akun</p>
			</a>
			</li>
			<li class="nav-item">
			<a href="{{ url('/user') }}" class="nav-link">
				<i class="far fa-circle nav-icon"></i>
				<p>User</p>
			</a>
			</li>
		</ul>
		</li>
		@endif
		@if (Session::get('user')['id_akses'] == 1 || Session::get('user')['id_akses'] == 4)
		<li class="nav-header">Transaksi</li>
		<li class="nav-item">
		<a href="{{ url('/kasMasuk') }}" class="nav-link">
			<i class="nav-icon far fa-image"></i>
			<p>
			Kas Masuk
			</p>
		</a>
		</li>
		<li class="nav-item">
		<a href="{{ url('/kasKeluar') }}" class="nav-link">
			<i class="nav-icon far fa-image"></i>
			<p>
			Kas Keluar
			</p>
		</a>
		</li>
		<li class="nav-header">Laporan</li>
		<li class="nav-item">
		<a href="{{ url('/kasMasuk/jurnalKasMasuk') }}" class="nav-link">
			<i class="nav-icon far fa-circle text-danger"></i>
			<p class="text">Jurnas Kas Masuk</p>
		</a>
		</li>
		<li class="nav-item">
		<a href="{{ url('/kasKeluar/jurnalKasKeluar') }}" class="nav-link">
			<i class="nav-icon far fa-circle text-warning"></i>
			<p>Jurnal Kas Keluar</p>
		</a>
		</li>
		<li class="nav-item">
		<a href="{{ url('/jurnal') }}" class="nav-link">
			<i class="nav-icon far fa-circle text-info"></i>
			<p>Jurnal</p>
		</a>
		</li>
		@endif
	</ul>
	</nav>
	<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->