@extends('layouts.main')
@section('content')

<div style="display: none">
	{{ $grandTotalDebit = 0 }}
	{{ $grandTotalKredit = 0 }}
</div>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Jurnal</h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
					<li class="breadcrumb-item active">Jurnal</li>
				</ol>
			</div>
		</div>
	</div>
</section>

<section class="content">
	<div class="container-fluid">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<div class="row">
					<div class="col-md-4">
						<form class="form-horizontal" action="{{ url('/jurnal/doSearch')}}" method="post">
							{{ csrf_field() }}
							<div class="form-group row">
								<label for="start" class="col-sm-2 col-form-label">Periode</label>
								<div class="col-sm-10">
									<input type="date" class="form-control" id="start" name="start" value="{{ $start }}" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="end" class="col-sm-2 col-form-label">&nbsp;</label>
								<div class="col-sm-10">
									<input type="date" class="form-control" id="end" name="end" value="{{ $end }}" required>
								</div>
							</div>
							<div class="form-group row">
								<label for="end" class="col-sm-2 col-form-label">&nbsp;</label>
								<div class="col-sm-10">
									<button type="submit" class="btn btn-info">Search</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<p>Search : {{$start}} - {{$end}}</p>
			</div>
			<div class="card-body">
				<table class="table table-bordered">
					<tr>
						<th>Tanggal</th>
						<th>Keterangan</th>
						<th>Reff</th>
						<th>Debit</th>
						<th>Kredit</th>
					</tr>
					@forelse($data as $key => $row)
					<tr>
						<td>{{$key}}</td>
						<td>
							@forelse($row as $child)
							<p>
								<a href="detail/{{$child->source}}/{{$child->id}}/{{$child->jenis_akun}}/{{$child->nama_akun}}">{{$child->nama_akun}}</a>
							</p>

							@if($child->nama_akun == 'Kas Kecil')
							<p style="text-align: right;">Kas Bank</p>
							@elseif($child->nama_akun == 'Pengembalian Kas')
							<p style="text-align: right;">Other Kas</p>
							@elseif($child->nama_akun == 'Biaya Sewa' || $child->nama_akun == 'Biaya Promosi' || $child->nama_akun == 'Biaya TOL, Parkir, Bensin' || $child->nama_akun == 'Pembelian Barang' || $child->nama_akun == 'Biaya Lain-lain')
							<p style="text-align: right;">Kas Kecil</p>
							@endif
							@empty
							<p>No Data</p>
							@endforelse
						</td>
						<td></td>
						<td>
							@forelse($row as $child)
							<p>
								@php
								echo App\Http\Controllers\KasMasukController::rp($child->nominal);
								@endphp
							</p>
							<p>&nbsp;</p>
							<div style="display: none">{{$grandTotalDebit += $child->nominal}}</div>
							@empty
							<p>No Data</p>
							@endforelse
						</td>
						<td>
							@forelse($row as $child)
							<p>&nbsp;</p>
							<p>
								@php
								echo App\Http\Controllers\KasMasukController::rp($child->nominal);
								@endphp
							</p>
							<div style="display: none">{{$grandTotalKredit += $child->nominal}}</div>
							@empty
							<p>No Data</p>
							@endforelse
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="100">No data.</td>
					</tr>
					@endforelse
					<tr>
						<th colspan="3">
							Total
						</th>
						<th>
							@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalDebit);
							@endphp
						</th>
						<th>
							@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalKredit);
							@endphp
						</th>
					</tr>
				</table>
				<br /><br />
				<p>
					<a class="btn btn-primary btn-lg" href="print/download/{{$s}}/{{$e}}">Cetak</a>
				</p>
			</div>
		</div>
	</div>
</section>

@endsection