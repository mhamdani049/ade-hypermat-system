@extends('layouts.main')
@section('content')

	<div style="display: none">
		{{ $grandTotalDebit = 0 }}
		{{ $grandTotalKredit = 0 }}
	</div>

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Jurnal</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              <li class="breadcrumb-item active">Jurnal</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
		  <div class="card-header">
			<div class="row">
				<div class="col-md-4">
					
				</div>
			</div>
          </div>
          <div class="card-body">
		  	<table class="table table-bordered">
			  	<tr>
					<th>Tanggal</th>
					<th>Keterangan</th>
					<th>Reff</th>
					<th>Debit</th>
					<th>Kredit</th>
				</tr>  
				@forelse($data as $key => $row)
					<tr>
						<td>{{$key}}</td>
						<td>
							@forelse($row as $child)
								@if($child->jenis_akun == 'Debit')
									<p>{{$child->nama_akun}}</p>
								@else
									<p style="text-align: right;">{{$child->nama_akun}}</p>
								@endif

								@if($child->jenis_akun == 'Debit' && $child->nama_akun == 'Kas Kecil')
									<p style="text-align: right;">Kas Bank</p>
								@endif
								@if($child->jenis_akun == 'Debit' && $child->nama_akun == 'Pengembalian Kas')
									<p style="text-align: right;">Other Kas</p>
								@endif
							@empty
								<p>No Data</p>
							@endforelse
						</td>
						<td></td>
						<td>
							@forelse($row as $child)
								@if($child->jenis_akun == 'Debit')
									<p>
										@php
											echo App\Http\Controllers\KasMasukController::rp($child->nominal);
										@endphp
									</p>
									<div style="display: none">{{$grandTotalDebit += $child->nominal}}</div>
								@else
									<p>&nbsp;</p>
								@endif
							@empty
								<p>No Data</p>
							@endforelse
						</td>
						<td>
							@forelse($row as $child)
								@if($child->jenis_akun == 'Kredit')
									<p>
										@php
											echo App\Http\Controllers\KasMasukController::rp($child->nominal);
										@endphp
									</p>
									<div style="display: none">{{$grandTotalKredit += $child->nominal}}</div>
								@else
									<p>&nbsp;</p>
								@endif

								@if($child->jenis_akun == 'Debit' && $child->nama_akun == 'Kas Kecil')
									<p>
										@php
											echo App\Http\Controllers\KasMasukController::rp($child->nominal);
										@endphp
									</p>
									<div style="display: none">{{$grandTotalKredit += $child->nominal}}</div>
								@endif
								@if($child->jenis_akun == 'Debit' && $child->nama_akun == 'Pengembalian Kas')
									<p>
										@php
											echo App\Http\Controllers\KasMasukController::rp($child->nominal);
										@endphp
									</p>
								@endif
							@empty
								<p>No Data</p>
							@endforelse
						</td>
					</tr>
				@empty
					<tr>
						<td colspan="100">No data.</td>
					</tr>
				@endforelse
				<tr>
					<th colspan="3">
						Total
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalDebit);
						@endphp
					</th>
					<th>
						@php
							echo App\Http\Controllers\KasMasukController::rp($grandTotalKredit);
						@endphp
					</th>
				</tr>
			</table>
          </div>
        </div>
      </div>
    </section>

@endsection

