@extends('layouts.main')
@section('content')

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Jurnal Detail</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              <li class="breadcrumb-item active">Jurnal Detail</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
		  <div class="card-header">

          </div>
          <div class="card-body">
		  	<table class="table table-bordered" width="100%">
			  	<tr>
					<td>Tanggal</td>
					<td> : </td>
					<td>{{$data['tanggal']}}</td>
				</tr>
				<tr>
					<td>Jenis Akun</td>
					<td> : </td>
					<td>{{$data['jenis_akun']}}</td>
				</tr>
				<tr>
					<td>Nama Akun</td>
					<td> : </td>
					<td>{{$data['nama_akun']}}</td>
				</tr>
				<tr>
					<td>Nominal</td>
					<td> : </td>
					<td>{{$data['nominal']}}</td>
				</tr>
				<tr>
					<td>Keterangan</td>
					<td> : </td>
					<td>{{$data['keterangan']}}</td>
				</tr>
			</tabel>
          </div>
        </div>
      </div>
    </section>

@endsection

