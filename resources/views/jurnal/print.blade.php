<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		body {font-size: 6; width: 100%;}
		table {border-collapse: collapse;}
		td    {padding: 0px;}
	</style>
	<div style="display: none">
		{{ $grandTotalDebit = 0 }}
		{{ $grandTotalKredit = 0 }}
	</div>
	<table class="table table-bordered" width="100%">
		<tr>
			<td>
				<img src="{!!asset('img/hypermart-1.png')!!}" width="180" />
			</td>
			<td colspan="4">
				<h7>Jurnal Kas Umum</h7>
				<p>Hypermart Sentul ({{$s}} s/d {{$e}})</p>
			</td>
		</tr>
		<tr>
			<th>Tanggal</th>
			<th>Keterangan</th>
			<th>Reff</th>
			<th>Debit</th>
			<th>Kredit</th>
		</tr>  
		@forelse($data as $key => $row)
			<tr>
				<td>{{$key}}</td>
				<td>
					@forelse($row as $child)
						<p>{{$child->nama_akun}}</p>
						
						@if($child->nama_akun == 'Kas Kecil')
							<p style="text-align: right;">Kas Bank</p>
						@elseif($child->nama_akun == 'Pengembalian Kas')
							<p style="text-align: right;">Other Kas</p>
						@elseif($child->nama_akun == 'Biaya Sewa' || $child->nama_akun == 'Biaya Promosi' || $child->nama_akun == 'Biaya TOL, Parkir, Bensin' || $child->nama_akun == 'Pembelian Barang' || $child->nama_akun == 'Biaya Lain-lain')
							<p style="text-align: right;">Kas Kecil</p>
						@endif
					@empty
						<p>No Data</p>
					@endforelse
				</td>
				<td></td>
				<td>
					@forelse($row as $child)
						<p>
							@php
								echo App\Http\Controllers\KasMasukController::rp($child->nominal);
							@endphp
						</p>
						<p>&nbsp;</p>
						<div style="display: none">{{$grandTotalDebit += $child->nominal}}</div>
					@empty
						<p>No Data</p>
					@endforelse
				</td>
				<td>
					@forelse($row as $child)
						<p>&nbsp;</p>
						<p>
							@php
								echo App\Http\Controllers\KasMasukController::rp($child->nominal);
							@endphp
						</p>
						<div style="display: none">{{$grandTotalKredit += $child->nominal}}</div>
					@empty
						<p>No Data</p>
					@endforelse
				</td>
			</tr>
		@empty
			<tr>
				<td colspan="100">No data.</td>
			</tr>
		@endforelse
		<tr>
			<th colspan="3">
				Total
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalDebit);
				@endphp
			</th>
			<th>
				@php
					echo App\Http\Controllers\KasMasukController::rp($grandTotalKredit);
				@endphp
			</th>
		</tr>
	</table>
</body>
</html>