@extends('layouts.main')
@section('content')

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pengajuan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              <li class="breadcrumb-item active">Pengajuan</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
          <div class="card-header">
		  	<a class="btn btn-success" href="{{ url('/pengajuanDana/add') }}">Add New</a>
          </div>
          <div class="card-body">
		  	@if(\Session::has('alert'))
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fas fa-ban"></i> Error</h5>
				{{Session::get('alert')}}
			</div>
			@endif
			@if(\Session::has('alert-success'))
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fas fa-ban"></i> Success</h5>
				{{Session::get('alert-success')}}
			</div>
			@endif
		  	<table class="table table-bordered" id="dt-table">
				<thead>
					<tr>
						<th>Id</th>
						<th>Tanggal PJD</th>
						<th>Nominal PJD</th>
						<th>Status</th>
						<th>Reason</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
          </div>
        </div>
      </div>
    </section>

	<script type="text/javascript">
		var id_akses = "{{Session::get('user')['id_akses']}}";

		$(function() {
			var dataTable = $('#dt-table').DataTable({
				processing: true,
				serverSide: true,
				order: [[ 0, "desc" ]],
				ajax: 'pengajuanDana/json',
				columns: [
					{ data: 'id_pengajuan', name: 'id_pengajuan' },
					{ data: 'tanggal_pjd', name: 'tanggal_pjd' },
					{ data: 'nominal_pjd', name: 'nominal_pjd' },
					{ data: 'status', name: 'status' },
					{ data: 'reason', name: 'reason' }
				],
				"columnDefs": [{
					"targets": 5,
					"data": null,
					"render": function(data, type, row, meta) {
						var button = '<button id="edit" class="btn btn-warning btn-sm">Edit</button> <button id="delete" class="btn btn-danger btn-sm">Delete</button> <button id="print" class="btn btn-info btn-sm">Cetak</button>';
						if (row.status === 1) button = '<button id="print" class="btn btn-info btn-sm">Cetak</button>';
						else if (row.status === 2) button = '<button id="delete" class="btn btn-danger btn-sm">Delete</button>';
						return button;
					}
				},{ 
					"targets": 3,
					"render": function(data, type, row, meta) {
						var status = '<p class="text-primary">Pending</p>';
						if (data === 1) status = '<p class="text-success">Approved</p>';
						else if (data === 2) status = '<p class="text-danger">Rejected</p>';
						return status;  
					}
				},{ 
					"targets": 2,
					"render": function(data, type, row, meta) {
						return numberComma(data); 
					}
				}]
			});

			$('#dt-table tbody').on('click', '#edit', function () {
				var data = dataTable.row( $(this).parents('tr') ).data();
				window.location.href = "pengajuanDana/edit/"+ data.id_pengajuan;
			});

			$('#dt-table tbody').on('click', '#delete', function () {
				var data = dataTable.row( $(this).parents('tr') ).data();
				var confirm = window.confirm("Apakah Anda yakin akan menghapus data ini?");
				if (confirm == true) {  
					window.location.href = "pengajuanDana/delete/"+ data.id_pengajuan;        
				}
			});

			$('#dt-table tbody').on('click', '#print', function () {
				var data = dataTable.row( $(this).parents('tr') ).data();
				window.location.href = "pengajuanDana/print/"+ data.id_pengajuan
			});
			
		});

		function numberComma(num) {
			return Math.round(num).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
		}
    
	</script>

@endsection
