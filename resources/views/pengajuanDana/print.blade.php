<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table {border-collapse: collapse;}
		td    {padding: 6px;}
	</style>
	<table border="0">
		<tr>
			<td><img src="{!!asset('img/hypermart-1.png')!!}" width="180" /></td>
			<td colspan="2">Tanda Terima Pengajuan Dana</td>
		</tr>
		<tr>
			<td>Kode Pengajuan</td>
			<td> : </td>
			<td>{{$data->id_pengajuan}}</td>
		</tr>
		<tr>
			<td>Tanggal</td>
			<td> : </td>
			<td>{{$data->tanggal_pjd}}</td>
		</tr>
		<tr>
			<td>Nominal</td>
			<td> : </td>
			<td>{{$data->nominal_pjd}}<br />({{$data->terbilang}})</td>
		</tr>
		<tr>
			<td>Keterangan</td>
			<td> : </td>
			<td>.....................................................................................</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td></td>
			<td>.....................................................................................</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td></td>
			<td>.....................................................................................</td>
		</tr>
		<tr>
			<td colspan="2">
				(Personalia)<br /><br /><br />
				(Admin)
			</td>
			<td>
				({{$data->created_by}})<br /><br /><br />
				({{$data->jabatan}})
			</td>
		</tr>
	</table>
</body>
</html>