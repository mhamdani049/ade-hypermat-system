@extends('layouts.main')
@section('content')

    <section class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
         		 <div class="col-sm-6">
            		<h1>Pengajuan</h1>
          		</div>
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              			<li class="breadcrumb-item"><a href="{{ url('/pengajuanDana') }}">Pengajuan</a></li>
			  			<li class="breadcrumb-item active">Add New</li>
            		</ol>
          		</div>
        	</div>
      	</div>
    </section>

    <section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Form</h3>
			</div>
			<form class="form-horizontal" action="{{ url('/pengajuanDana/doAdd')}}" method="post">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group row">
					<label for="tanggal_pjd" class="col-sm-2 col-form-label">Tanggal PDK</label>
					<div class="col-sm-10">
						<input type="date" class="form-control" id="tanggal_pjd" name="tanggal_pjd" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="nominal_pjd" class="col-sm-2 col-form-label">Nominal PDK</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nominal_pjd" name="nominal_pjd" required>
					</div>
				</div>
			</div>
				<div class="card-footer">
					<button type="submit" class="btn btn-info">Simpan</button>
					<a class="btn btn-default float-right" href="{{ url('/pengajuanDana') }}">Batal</a>
				</div>
			</form>
			</div>
		</div>
    </section>

@endsection
