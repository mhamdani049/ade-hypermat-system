@extends('layouts.main')
@section('content')

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Akun</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              <li class="breadcrumb-item active">Akun</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
          <div class="card-header">
		  	<a class="btn btn-success" href="{{ url('/akun/add') }}">Add New</a>
          </div>
          <div class="card-body">
		  	@if(\Session::has('alert'))
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fas fa-ban"></i> Error</h5>
				{{Session::get('alert')}}
			</div>
			@endif
			@if(\Session::has('alert-success'))
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fas fa-ban"></i> Success</h5>
				{{Session::get('alert-success')}}
			</div>
			@endif
		  	<table class="table table-bordered" id="dt-table">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nama Akun</th>
						<th>Jenis Akun</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
          </div>
        </div>
      </div>
    </section>

	<script type="text/javascript">
		$(function() {
			var dataTable = $('#dt-table').DataTable({
				processing: true,
				serverSide: true,
				order: [[ 0, "desc" ]],
				ajax: 'akun/json',
				columns: [
					{ data: 'id_akun', name: 'id_akun' },
					{ data: 'nama_akun', name: 'nama_akun' },
					{ data: 'jenis_akun', name: 'jenis_akun' }
				],
				"columnDefs": [{
					"targets": 3,
					"data": null,
					"defaultContent": '<button id="edit" class="btn btn-primary btn-sm">Edit</button> <button id="delete" class="btn btn-danger btn-sm">Delete</button>'
				}]
			});

			$('#dt-table tbody').on('click', '#edit', function () {
				var data = dataTable.row( $(this).parents('tr') ).data();
				window.location.href = "akun/edit/"+ data.id_akun;
			});

			$('#dt-table tbody').on('click', '#delete', function () {
				var data = dataTable.row( $(this).parents('tr') ).data();
				var confirm = window.confirm("Apakah Anda yakin akan menghapus data ini?");
				if (confirm == true) {  
					window.location.href = "akun/delete/"+ data.id_akun;        
				}
			});
		});
	</script>

@endsection
