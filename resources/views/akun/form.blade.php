@extends('layouts.main')
@section('content')

    <section class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
         		 <div class="col-sm-6">
            		<h1>Akun</h1>
          		</div>
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
              			<li class="breadcrumb-item"><a href="{{ url('/akun') }}">Akun</a></li>
			  			<li class="breadcrumb-item active">Add New</li>
            		</ol>
          		</div>
        	</div>
      	</div>
    </section>

    <section class="content">
		<div class="container-fluid">
			<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Form</h3>
			</div>
			<form class="form-horizontal" action="{{ url('/akun/doAdd')}}" method="post">
			{{ csrf_field() }}
			<div class="card-body">
				<div class="form-group row">
					<label for="nama_akun" class="col-sm-2 col-form-label">Nama Akun</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nama_akun" name="nama_akun" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="jenis_akun" class="col-sm-2 col-form-label">Jenis Akun</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="jenis_akun" name="jenis_akun" required>
					</div>
				</div>
			</div>
				<div class="card-footer">
					<button type="submit" class="btn btn-info">Simpan</button>
					<a class="btn btn-default float-right" href="{{ url('/akun') }}">Batal</a>
				</div>
			</form>
			</div>
		</div>
    </section>

@endsection
