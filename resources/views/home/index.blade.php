@extends('layouts.main')
@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
	<div class="container-fluid">
	<div class="row mb-2">
		<div class="col-sm-6">
		<h1 class="m-0 text-dark">Dashboard</h1>
		</div><!-- /.col -->
		<div class="col-sm-6">
		<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
			<li class="breadcrumb-item active">Main</li>
		</ol>
		</div><!-- /.col -->
	</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		@if (Session::get('user')['id_akses'] == 1 || Session::get('user')['id_akses'] == 4)
		<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-info">
			<div class="inner">
			<h3>{{ $data->total_pencairan }}</h3>

			<p>Pencairan Dana</p>
			</div>
			<div class="icon">
			<i class="ion ion-bag"></i>
			</div>
			<a href="{{ url('/pencairanDana') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-success">
			<div class="inner">
			<h3>{{ $data->total_pengajuan }}</h3>

			<p>Pengajuan Dana</p>
			</div>
			<div class="icon">
			<i class="ion ion-stats-bars"></i>
			</div>
			<a href="{{ url('/pengajuanDana') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
		</div>
		@endif
		<!-- ./col -->
		<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-warning">
			<div class="inner">
			<h3>{{ $data->total_kas_masuk }}</h3>

			<p>Transaksi Kas Masuk</p>
			</div>
			<div class="icon">
			<i class="ion ion-person-add"></i>
			</div>
			<a href="{{ url('/kasMasuk') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-6">
		<!-- small box -->
		<div class="small-box bg-danger">
			<div class="inner">
			<h3>{{ $data->total_kas_keluar }}</h3>

			<p>Transaksi Kas Keluar</p>
			</div>
			<div class="icon">
			<i class="ion ion-pie-graph"></i>
			</div>
			<a href="{{ url('/kasKeluar') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->
	<!-- /.row (main row) -->
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection