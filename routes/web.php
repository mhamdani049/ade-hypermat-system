<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@showIndex');
Route::get('/login', 'UsersController@showLogin');
Route::post('/login', 'UsersController@doLogin');
Route::get('/logout', 'UsersController@doLogout');

Route::get('/pencairanDana/json','PencairanDanaController@json');
Route::get('/pencairanDana', 'PencairanDanaController@showIndex');
Route::get('/pencairanDana/add', 'PencairanDanaController@showAdd');
Route::post('/pencairanDana/doAdd', 'PencairanDanaController@doAdd');
Route::get('/pencairanDana/edit/{id}', 'PencairanDanaController@showEdit');
Route::post('/pencairanDana/doUpdate', 'PencairanDanaController@doUpdate');
Route::get('/pencairanDana/delete/{id}', 'PencairanDanaController@doDelete');
Route::get('/pencairanDana/print/{id}', 'PencairanDanaController@doPrint');

Route::get('/akun/json','AkunController@json');
Route::get('/akun', 'AkunController@showIndex');
Route::get('/akun/add', 'AkunController@showAdd');
Route::post('/akun/doAdd', 'AkunController@doAdd');
Route::get('/akun/edit/{id}', 'AkunController@showEdit');
Route::post('/akun/doUpdate', 'AkunController@doUpdate');
Route::get('/akun/delete/{id}', 'AkunController@doDelete');

Route::get('/kasMasuk/json','KasMasukController@json');
Route::get('/kasMasuk', 'KasMasukController@showIndex');
Route::get('/kasMasuk/edit/{id}', 'KasMasukController@showEdit');
Route::post('/kasMasuk/doUpdate', 'KasMasukController@doUpdate');
Route::get('/kasMasuk/jurnalKasMasuk', 'KasMasukController@showJurnalKasMasuk');
Route::post('/kasMasuk/doSearchJurnalKasMasuk', 'KasMasukController@doSearchJurnalKasMasuk');
Route::get('/kasMasuk/print/{source}/{start}/{end}', 'KasMasukController@doPrint');

Route::get('/pengajuanDana/json','PengajuanDanaController@json');
Route::get('/pengajuanDana', 'PengajuanDanaController@showIndex');
Route::get('/pengajuanDana/add', 'PengajuanDanaController@showAdd');
Route::post('/pengajuanDana/doAdd', 'PengajuanDanaController@doAdd');
Route::get('/pengajuanDana/edit/{id}', 'PengajuanDanaController@showEdit');
Route::post('/pengajuanDana/doUpdate', 'PengajuanDanaController@doUpdate');
Route::get('/pengajuanDana/delete/{id}', 'PengajuanDanaController@doDelete');
Route::get('/pengajuanDana/print/{id}', 'PengajuanDanaController@doPrint');

Route::get('/kasKeluar/json','KasKeluarController@json');
Route::get('/kasKeluar', 'KasKeluarController@showIndex');
Route::get('/kasKeluar/edit/{id}', 'KasKeluarController@showEdit');
Route::post('/kasKeluar/doUpdate', 'KasKeluarController@doUpdate');
Route::get('/kasKeluar/jurnalKasKeluar', 'KasKeluarController@showJurnalKasKeluar');
Route::post('/kasKeluar/doSearchJurnalKasKeluar', 'KasKeluarController@doSearchJurnalKasKeluar');
Route::get('/kasKeluar/print/{source}/{start}/{end}', 'KasKeluarController@doPrint');

Route::get('/user', 'UsersController@showIndex');
Route::get('/user/add', 'UsersController@showAdd');
Route::post('/user/doAdd', 'UsersController@doAdd');
Route::get('/user/edit/{id}/{table}', 'UsersController@showEdit');
Route::post('/user/doUpdate', 'UsersController@doUpdate');
Route::get('/user/changePassword/{id}/{table}', 'UsersController@showChangePassword');
Route::post('/user/doChangePassword', 'UsersController@doChangePassword');
Route::get('/user/delete/{id}/{table}', 'UsersController@doDelete');

Route::get('/jurnal', 'JurnalController@showIndex');
Route::post('/jurnal/doSearch', 'JurnalController@doSearch');
Route::get('/jurnal/print/{source}/{start}/{end}', 'JurnalController@doPrint');
Route::get('/jurnal/detail/{table}/{id}/{jenis_akun}/{nama_akun}', 'JurnalController@showDetail');