-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: ade_hypermart_system
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id_pengguna_adm` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password_adm` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `id_akses` int(11) DEFAULT NULL,
  `jabatan` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pengguna_adm`)
) ENGINE=InnoDB AUTO_INCREMENT=444448 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (444444,'admin','$2y$10$KzmwARlgwUcO1YAXOF67EuA1bBle1A9crAt2b0PE27xeE6aUuR1Km',4,'Admin','2020-07-26 20:48:16','2020-08-08 07:22:04');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `akun`
--

DROP TABLE IF EXISTS `akun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL AUTO_INCREMENT,
  `nama_akun` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `jenis_akun` varchar(11) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_akun`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `akun`
--

LOCK TABLES `akun` WRITE;
/*!40000 ALTER TABLE `akun` DISABLE KEYS */;
INSERT INTO `akun` VALUES (1,'Kas Kecil','Debit','2020-08-06 13:37:56','2020-08-06 13:37:56'),(2,'Pengembalian Kas','Debit','2020-08-06 13:38:06','2020-08-06 13:38:06'),(3,'Kas Bank','Kredit','2020-08-06 13:39:10','2020-08-06 13:39:10'),(4,'Biaya Promosi','Kredit','2020-08-06 13:39:20','2020-08-06 13:39:20'),(5,'Biaya Sewa','Kredit','2020-08-06 13:39:29','2020-08-06 13:39:29'),(6,'Biaya TOL, Parkir, Bensin','Kredit','2020-08-06 13:39:39','2020-08-06 13:39:39'),(7,'Pembelian Barang','Kredit','2020-08-06 13:39:49','2020-08-06 13:39:49'),(8,'Biaya Lain-lain','Kredit','2020-08-06 13:39:59','2020-08-06 13:39:59'),(9,'Other Kas','Kredit','2020-08-06 13:40:08','2020-08-06 13:40:08');
/*!40000 ALTER TABLE `akun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departemen_lain`
--

DROP TABLE IF EXISTS `departemen_lain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departemen_lain` (
  `id_pengguna_dl` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password_dl` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `id_akses` int(11) DEFAULT NULL,
  `jabatan` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pengguna_dl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departemen_lain`
--

LOCK TABLES `departemen_lain` WRITE;
/*!40000 ALTER TABLE `departemen_lain` DISABLE KEYS */;
INSERT INTO `departemen_lain` VALUES ('111111','Departemen Lain','$2y$10$GgBAmoXEflCh.6akkk6wEuBNNP5nPzMsNn8bHnSx/d2r3BvdtsZgi',3,'Manager','2020-07-12 17:34:40','2020-08-08 07:22:00');
/*!40000 ALTER TABLE `departemen_lain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance`
--

DROP TABLE IF EXISTS `finance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `finance` (
  `id_pengguna_fe` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password_fe` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `id_akses` int(11) DEFAULT NULL,
  `jabatan` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pengguna_fe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance`
--

LOCK TABLES `finance` WRITE;
/*!40000 ALTER TABLE `finance` DISABLE KEYS */;
INSERT INTO `finance` VALUES ('222222','Finance','$2y$10$KzmwARlgwUcO1YAXOF67EuA1bBle1A9crAt2b0PE27xeE6aUuR1Km',2,'Manager','2020-07-12 17:35:11','2020-08-08 07:21:12');
/*!40000 ALTER TABLE `finance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hak_akses`
--

DROP TABLE IF EXISTS `hak_akses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hak_akses` (
  `id_akses` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_akses`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hak_akses`
--

LOCK TABLES `hak_akses` WRITE;
/*!40000 ALTER TABLE `hak_akses` DISABLE KEYS */;
INSERT INTO `hak_akses` VALUES (1,'Personalia','2020-07-09 00:00:00',NULL),(2,'Finance','2020-07-09 00:00:00',NULL),(3,'Departemen Lain','2020-07-09 00:00:00',NULL),(4,'Super Admin','2020-07-26 20:44:49',NULL);
/*!40000 ALTER TABLE `hak_akses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2019_08_19_000000_create_failed_jobs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pencairan_dana`
--

DROP TABLE IF EXISTS `pencairan_dana`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pencairan_dana` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pencairan` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `tanggal_pdk` date DEFAULT NULL,
  `nominal_pdk` int(11) DEFAULT NULL,
  `id_pengguna_fe` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0 = Pending\n1 = Approve\n2 = Reject',
  `created_by` varchar(15) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `reason` text COLLATE utf8mb4_general_ci,
  PRIMARY KEY (`id`,`id_pencairan`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pencairan_dana`
--

LOCK TABLES `pencairan_dana` WRITE;
/*!40000 ALTER TABLE `pencairan_dana` DISABLE KEYS */;
INSERT INTO `pencairan_dana` VALUES (1,'PDK06082001','2020-08-01',5000000,222222,1,NULL,'2020-08-13 00:55:49','2020-08-06 13:44:01',NULL),(2,'PDK06082002','2020-08-05',45000,222222,1,NULL,'2020-08-13 00:56:26','2020-08-06 13:44:27',NULL),(3,'PDK06082003','2020-08-10',15000,222222,1,NULL,'2020-08-13 00:57:03','2020-08-06 13:44:53',NULL),(4,'PDK06082004','2020-08-13',20000,222222,1,NULL,'2020-08-13 00:57:29','2020-08-06 13:45:07',NULL),(5,'PDK06082005','2020-08-15',7000000,222222,1,NULL,'2020-08-13 01:31:16','2020-08-06 13:45:26',NULL),(6,'PDK06082006','2020-08-23',25000,222222,1,NULL,'2020-08-13 01:32:12','2020-08-06 13:52:57',NULL),(7,'PDK06082007','2020-08-30',45000,222222,1,NULL,'2020-08-13 01:32:41','2020-08-06 13:53:09',NULL),(8,'PDK06082008','2020-09-01',20000,222222,0,NULL,'2020-08-06 15:40:03','2020-08-06 15:25:45',NULL),(10,'PDK08082010','2020-08-08',2000000,222222,0,NULL,'2020-08-08 06:36:45','2020-08-08 06:34:03',NULL),(11,'PDK08082011','2020-08-08',4000000,222222,0,NULL,'2020-08-08 06:45:10','2020-08-08 06:44:36',NULL);
/*!40000 ALTER TABLE `pencairan_dana` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengajuan_dana`
--

DROP TABLE IF EXISTS `pengajuan_dana`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pengajuan_dana` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengajuan` varchar(11) COLLATE utf8mb4_general_ci NOT NULL,
  `tanggal_pjd` date DEFAULT NULL,
  `nominal_pjd` int(11) DEFAULT NULL,
  `id_pengguna_dl` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_by` varchar(15) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `reason` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`id_pengajuan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengajuan_dana`
--

LOCK TABLES `pengajuan_dana` WRITE;
/*!40000 ALTER TABLE `pengajuan_dana` DISABLE KEYS */;
INSERT INTO `pengajuan_dana` VALUES (1,'PDK13082001','2020-08-03',230000,'333333',1,NULL,NULL,'2020-08-13 00:58:06','2020-08-13 00:59:00'),(2,'PDK13082002','2020-08-03',125000,'333333',1,NULL,NULL,'2020-08-13 00:58:31','2020-08-13 01:19:00'),(3,'PDK13082003','2020-08-04',500000,'333333',1,NULL,NULL,'2020-08-13 01:33:20','2020-08-13 01:33:52'),(4,'PDK13082004','2020-08-07',400000,'333333',1,NULL,NULL,'2020-08-13 01:33:32','2020-08-13 01:34:07');
/*!40000 ALTER TABLE `pengajuan_dana` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personalia`
--

DROP TABLE IF EXISTS `personalia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personalia` (
  `id_pengguna` varchar(10) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `id_akses` int(11) DEFAULT NULL,
  `jabatan` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pengguna`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personalia`
--

LOCK TABLES `personalia` WRITE;
/*!40000 ALTER TABLE `personalia` DISABLE KEYS */;
INSERT INTO `personalia` VALUES ('333333','Personalia','$2y$10$LAAHlc117nMGOJKWARRtV.LdDxWjcAD3ZP7TgJTo/Sd81AO6tqn52',1,'Supervisor','2020-07-09 00:00:00','2020-08-08 07:21:55');
/*!40000 ALTER TABLE `personalia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_kas_keluar`
--

DROP TABLE IF EXISTS `transaksi_kas_keluar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_kas_keluar` (
  `id_tkk` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `id_akun` int(11) DEFAULT NULL,
  `id_pengajuan` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_general_ci,
  `created_by` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_tkk`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_kas_keluar`
--

LOCK TABLES `transaksi_kas_keluar` WRITE;
/*!40000 ALTER TABLE `transaksi_kas_keluar` DISABLE KEYS */;
INSERT INTO `transaksi_kas_keluar` VALUES (1,'2020-08-03',230000,5,'PDK13082001','Biaya Sewa Mobil','333333','2020-08-13 00:59:00','2020-08-13 00:59:00'),(2,'2020-08-03',125000,7,'PDK13082002','Pembelian ATK','333333','2020-08-13 01:19:00','2020-08-13 01:19:00'),(3,'2020-08-04',500000,4,'PDK13082003','Promsi Koran Mingguan','333333','2020-08-13 01:33:52','2020-08-13 01:33:52'),(4,'2020-08-07',400000,4,'PDK13082004','Promsi Koran Mingguan','333333','2020-08-13 01:34:07','2020-08-13 01:34:07');
/*!40000 ALTER TABLE `transaksi_kas_keluar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi_kas_masuk`
--

DROP TABLE IF EXISTS `transaksi_kas_masuk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi_kas_masuk` (
  `id_tkm` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `id_akun` int(11) DEFAULT NULL,
  `id_pencairan` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_general_ci,
  `created_by` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_tkm`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi_kas_masuk`
--

LOCK TABLES `transaksi_kas_masuk` WRITE;
/*!40000 ALTER TABLE `transaksi_kas_masuk` DISABLE KEYS */;
INSERT INTO `transaksi_kas_masuk` VALUES (1,'2020-08-01',5000000,1,'PDK06082001','Pencairan Dana','333333','2020-08-13 00:55:49','2020-08-13 00:55:49'),(2,'2020-08-05',45000,2,'PDK06082002','Kembalian dana Promosi','333333','2020-08-13 00:56:26','2020-08-13 00:56:26'),(3,'2020-08-10',15000,2,'PDK06082003','Kembalian dana Promosi','333333','2020-08-13 00:57:03','2020-08-13 00:57:03'),(4,'2020-08-13',20000,2,'PDK06082004','Kembalian dana Promosi','333333','2020-08-13 00:57:29','2020-08-13 00:57:29'),(5,'2020-08-15',7000000,1,'PDK06082005','Pencairan Dana','333333','2020-08-13 01:31:16','2020-08-13 01:31:16'),(6,'2020-08-23',25000,2,'PDK06082006','Kembalian dana Promosi','333333','2020-08-13 01:32:12','2020-08-13 01:32:12'),(7,'2020-08-30',45000,2,'PDK06082007','Kembalian dana Promosi','333333','2020-08-13 01:32:41','2020-08-13 01:32:41');
/*!40000 ALTER TABLE `transaksi_kas_masuk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-13 11:10:05
