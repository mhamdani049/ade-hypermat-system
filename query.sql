use ade_hypermart_system;

-- SELECT 
--     (SELECT 
--             COUNT(id_pencairan)
--         FROM
--             pencairan_dana) AS total_pencairan,
--     (SELECT 
--             COUNT(id_pengajuan)
--         FROM
--             pengajuan_dana) AS total_pengajuan,
--     (SELECT 
--             COUNT(id_tkm)
--         FROM
--             transaksi_kas_masuk) AS total_kas_masuk,
--     (SELECT 
--             COUNT(id_tkk)
--         FROM
--             transaksi_kas_keluar) AS total_kas_keluar
-- FROM DUAL;

select a.*, b.nama as name_akses from (
select id_pengguna_fe as id, name, password_fe as password, id_akses, created_at, updated_at, 'finance' as tb from finance
union 
select id_pengguna_dl as id, name, password_dl as password, id_akses, created_at, updated_at, 'departemen_lain' as tb from departemen_lain
union
select id_pengguna as id, name, password as password, id_akses, created_at, updated_at, 'personalia' as tb from personalia
union
select id_pengguna_adm as id, name, password_adm as password, id_akses, created_at, updated_at, 'admin' as tb from admin
) a inner join hak_akses b on a.id_akses = b.id_akses