<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\HakAkses;
use App\Personalia;
use App\Finance;
use App\DepartemenLain;
use App\Admin;

class UsersController extends Controller
{
	public function showIndex()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {

			$data = DB::select("
			select a.*, b.nama as name_akses from (
				select id_pengguna_fe as id, name, password_fe as password, id_akses, created_at, updated_at, 'finance' as tb from finance
				union 
				select id_pengguna_dl as id, name, password_dl as password, id_akses, created_at, updated_at, 'departemen_lain' as tb from departemen_lain
				union
				select id_pengguna as id, name, password as password, id_akses, created_at, updated_at, 'personalia' as tb from personalia
				union
				select id_pengguna_adm as id, name, password_adm as password, id_akses, created_at, updated_at, 'admin' as tb from admin
				) a inner join hak_akses b on a.id_akses = b.id_akses
			");
			
            return view('user/index', ['data' => $data]);
        }
	}

	public function showAdd()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
			$jabatan = ['Admin', 'Manager', 'Supervisor'];
            $hasAkses = HakAkses::get();
            return view('user/form', ['hasAkses' => $hasAkses, 'jabatan' => $jabatan]);
        }
	}
	
	public function doAdd(Request $request)
    {
		if ($request->id_akses == 1) {
			$personalia = new Personalia();
			$personalia['id_pengguna'] = $request->id;
			$personalia['name'] = $request->name;
			$personalia['password'] = bcrypt($request->password);
			$personalia['id_akses'] = $request->id_akses;
			$personalia['jabatan'] = $request->jabatan;

			try {
				$personalia->save();
			} catch (\Exception $e) {
				$error = $e->getMessage();
				return redirect('user')->with('alert', $error);
			}
		} else if ($request->id_akses == 2) {
			$finance = new Finance();
			$finance['id_pengguna_fe'] = $request->id;
			$finance['name'] = $request->name;
			$finance['password_fe'] = bcrypt($request->password);
			$finance['id_akses'] = $request->id_akses;
			$finance['jabatan'] = $request->jabatan;

			try {
				$finance->save();
			} catch (\Exception $e) {
				$error = $e->getMessage();
				return redirect('user')->with('alert', $error);
			}
		} else if ($request->id_akses == 3) {
			$departemenLain = new DepartemenLain();
			$departemenLain['id_pengguna_dl'] = $request->id;
			$departemenLain['name'] = $request->name;
			$departemenLain['password_dl'] = bcrypt($request->password);
			$departemenLain['id_akses'] = $request->id_akses;
			$departemenLain['jabatan'] = $request->jabatan;

			try {
				$departemenLain->save();
			} catch (\Exception $e) {
				$error = $e->getMessage();
				return redirect('user')->with('alert', $error);
			}
		} else {
			$admin = new Admin();
			$admin['id_pengguna_adm'] = $request->id;
			$admin['name'] = $request->name;
			$admin['password_adm'] = bcrypt($request->password);
			$admin['id_akses'] = $request->id_akses;
			$admin['jabatan'] = $request->jabatan;

			try {
				$admin->save();
			} catch (\Exception $e) {
				$error = $e->getMessage();
				return redirect('user')->with('alert', $error);
			}
		}
		
		return redirect('user')->with(
            'alert-success',
            'Berhasil simpan'
        );
	}
	
	public function showEdit($id, $table)
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
			$jabatan = ['Admin', 'Manager', 'Supervisor'];
			$hasAkses = HakAkses::get();
			if ($table == 1) {
				$data = Personalia::where('id_pengguna', $id)->first();
				$data['id'] = $data->id_pengguna;
			}
			else if ($table == 2) {
				$data = Finance::where('id_pengguna_fe', $id)->first();
				$data['id'] = $data->id_pengguna_fe;
			}
			else if ($table == 3) {
				$data = DepartemenLain::where('id_pengguna_dl', $id)->first();
				$data['id'] = $data->id_pengguna_dl;
			}
			else if ($table == 4) {
				$data = Admin::where('id_pengguna_adm', $id)->first();
				$data['id'] = $data->id_pengguna_adm;
			}
            return view('user/edit', ['data' => $data, 'hasAkses' => $hasAkses, 'jabatan' => $jabatan]);
        }
	}
	
	public function doUpdate(Request $request)
    {
        try {
			if ($request->_id_akses == 1) {
				Personalia::where('id_pengguna', $request->id)->update([
					'name' => $request->name,
					'jabatan' => $request->jabatan
				]);
			} else if ($request->_id_akses == 2) {
				Finance::where('id_pengguna_fe', $request->id)->update([
					'name' => $request->name,
					'jabatan' => $request->jabatan
				]);
			} else if ($request->_id_akses == 3) {
				DepartemenLain::where('id_pengguna_dl', $request->id)->update([
					'name' => $request->name,
					'jabatan' => $request->jabatan
				]);
			} else if ($request->_id_akses == 4) {
				Admin::where('id_pengguna_adm', $request->id)->update([
					'name' => $request->name,
					'jabatan' => $request->jabatan
				]);
			}
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('user')->with('alert', $error);
        }

        return redirect('user')->with('alert-success', 'Berhasil update');
	}
	
	public function showChangePassword($id, $table)
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
			$hasAkses = HakAkses::get();
			if ($table == 1) {
				$data = Personalia::where('id_pengguna', $id)->first();
				$data['id'] = $data->id_pengguna;
			}
			else if ($table == 2) {
				$data = Finance::where('id_pengguna_fe', $id)->first();
				$data['id'] = $data->id_pengguna_fe;
			}
			else if ($table == 3) {
				$data = DepartemenLain::where('id_pengguna_dl', $id)->first();
				$data['id'] = $data->id_pengguna_dl;
			}
			else if ($table == 4) {
				$data = Admin::where('id_pengguna_adm', $id)->first();
				$data['id'] = $data->id_pengguna_adm;
			}
            return view('user/changePassword', ['data' => $data, 'hasAkses' => $hasAkses]);
        }
	}

	public function doChangePassword(Request $request)
    {
        try {
			if ($request->_id_akses == 1) {
				Personalia::where('id_pengguna', $request->id)->update([
					'password' => bcrypt($request->password)
				]);
			} else if ($request->_id_akses == 2) {
				Finance::where('id_pengguna_fe', $request->id)->update([
					'password_fe' => bcrypt($request->password)
				]);
			} else if ($request->_id_akses == 3) {
				DepartemenLain::where('id_pengguna_dl', $request->id)->update([
					'password_dl' => bcrypt($request->password)
				]);
			} else if ($request->_id_akses == 4) {
				Admin::where('id_pengguna_adm', $request->id)->update([
					'password_adm' => bcrypt($request->password)
				]);
			}
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('user')->with('alert', $error);
        }

        return redirect('user')->with('alert-success', 'Berhasil update');
	}

	public function doDelete($id, $table)
    {
        try {
			if ($table == 1) {
				Personalia::where('id_pengguna', $id)->delete();
			} else if ($table == 2) {
				Finance::where('id_pengguna_fe', $id)->delete();
			} else if ($table == 3) {
				DepartemenLain::where('id_pengguna_dl', $id)->delete();
			} else if ($table == 4) {
				Admin::where('id_pengguna_adm', $id)->delete();
			}
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('user')->with('alert', $error);
        }

        return redirect('user')->with('alert-success', 'Berhasil hapus');
    }

    public function showLogin()
    {
        // $password = bcrypt('123qwe');
        // print_r($password);
        // exit;

        $hakAkses = HakAkses::get();
        return view('login', ['hak_akses' => $hakAkses]);
    }

    public function doLogout()
    {
        Session::flush();
        return redirect('login')->with('alert', 'Kamu sudah logout!');
    }

    public function doLogin(Request $request)
    {
        $id = $request->id;
        $password = $request->password;
        $id_akses = $request->id_akses;

        $personalia = Personalia::where('id_pengguna', $id)
            ->where('id_akses', $id_akses)
            ->first();
        if ($personalia) {
            if (Hash::check($password, $personalia->password)) {
                $user = [];
                $user['id'] = $personalia->id_pengguna;
                $user['name'] = $personalia->name;
                $user['id_akses'] = $personalia->id_akses;
                Session::put('user', $user);
                Session::put('login', true);
                return redirect('/');
            }
        } else {
            $finance = Finance::where('id_pengguna_fe', $id)
                ->where('id_akses', $id_akses)
                ->first();
            if ($finance) {
                if (Hash::check($password, $finance->password_fe)) {
                    $user = [];
                    $user['id'] = $finance->id_pengguna_fe;
                    $user['name'] = $finance->name;
                    $user['id_akses'] = $finance->id_akses;
                    Session::put('user', $user);
                    Session::put('login', true);
                    return redirect('/');
                }
            } else {
                $departemenLain = DepartemenLain::where('id_pengguna_dl', $id)
                    ->where('id_akses', $id_akses)
                    ->first();
                if ($departemenLain) {
                    if (Hash::check($password, $departemenLain->password_dl)) {
                        $user = [];
                        $user['id'] = $departemenLain->id_pengguna_dl;
                        $user['name'] = $departemenLain->name;
                        $user['id_akses'] = $departemenLain->id_akses;
                        Session::put('user', $user);
                        Session::put('login', true);
                        return redirect('/');
                    }
                } else {
					$admin = Admin::where('id_pengguna_adm', $id)
                    ->where('id_akses', $id_akses)
					->first();
					if ($admin) {
						if (Hash::check($password, $admin->password_adm)) {
							$user = [];
							$user['id'] = $admin->id_pengguna_adm;
							$user['name'] = $admin->name;
							$user['id_akses'] = $admin->id_akses;
							Session::put('user', $user);
							Session::put('login', true);
							return redirect('/');
						}
					} else {
						return redirect('login')->with(
							'alert',
							'ID atau password Anda salah'
						);
					}
                }
            }
        }
	}
	
	
}
