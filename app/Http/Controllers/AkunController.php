<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Akun;

use DataTables;

class AkunController extends Controller
{
    public function json()
    {
        return Datatables::of(Akun::all())->make(true);
    }

    public function showIndex()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('akun/index');
        }
    }

    public function showAdd()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('akun/form');
        }
    }

    public function doAdd(Request $request)
    {
        $Akun = new Akun();
        $Akun['nama_akun'] = $request->nama_akun;
        $Akun['jenis_akun'] = $request->jenis_akun;

        try {
            $Akun->save();
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('akun')->with('alert', $error);
        }

        return redirect('akun')->with('alert-success', 'Berhasil simpan');
    }

    public function showEdit($id)
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data = Akun::where('id_akun', $id)->first();
            return view('akun/edit', ['data' => $data]);
        }
    }

    public function doUpdate(Request $request)
    {
        try {
            Akun::where('id_akun', $request->id_akun)->update([
                'nama_akun' => $request->nama_akun,
                'jenis_akun' => $request->jenis_akun,
            ]);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('akun')->with('alert', $error);
        }

        return redirect('akun')->with('alert-success', 'Berhasil update');
    }

    public function doDelete($id)
    {
        try {
            Akun::where('id_akun', $id)->delete();
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('akun')->with('alert', $error);
        }

        return redirect('akun')->with('alert-success', 'Berhasil hapus');
    }
}
