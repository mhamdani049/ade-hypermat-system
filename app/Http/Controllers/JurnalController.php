<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\TransaksiKasMasuk;
use App\TransaksiKasKeluar;

use DataTables, PDF;

class JurnalController extends Controller
{
	public function showIndex()
	{
		if (!Session::get('login')) {
			return redirect('login')->with('alert', 'Kamu harus login dulu');
		} else {
			$s = date('Y-m-d') . ' 00:00:00';
			$e = date('Y-m-d') . ' 23:59:59';
			$data = DB::select("select x.* from (select a2.nama_akun, a.nominal, a2.jenis_akun, a.tanggal, a.id_tkm as id, 'kas_masuk' as source from transaksi_kas_masuk a inner join akun a2 on a.id_akun = a2.id_akun union select a2.nama_akun, a.nominal, a2.jenis_akun, a.tanggal, a.id_tkk as id, 'kas_keluar' as source from transaksi_kas_keluar a inner join akun a2 on a.id_akun = a2.id_akun) x where x.tanggal >= '" . $s . "' and x.tanggal <= '" . $e . "' ORDER BY x.tanggal ASC");

			$arr = array();

			foreach ($data as $key => $item) {
				$arr[$item->tanggal][$key] = $item;
			}

			ksort($arr, SORT_NUMERIC);

			return view('jurnal/index', ['data' => $arr, 'start' => date('d/m/Y'), 'end' => date('d/m/Y'), 's' => $s, 'e' => $e]);
		}
	}

	public function doSearch(Request $request)
	{
		if (!Session::get('login')) {
			return redirect('login')->with('alert', 'Kamu harus login dulu');
		} else {
			$s = date('Y-m-d') . ' 00:00:00';
			$e = date('Y-m-d') . ' 23:59:59';

			if (isset($request->start))
				$s = date("Y-m-d", strtotime($request->start)) . ' 00:00:00';

			if (isset($request->end))
				$e = date("Y-m-d", strtotime($request->end)) . ' 23:59:59';

			$data = DB::select("select x.* from (select a2.nama_akun, a.nominal, a2.jenis_akun, a.tanggal, a.id_tkm as id, 'kas_masuk' as source from transaksi_kas_masuk a inner join akun a2 on a.id_akun = a2.id_akun union select a2.nama_akun, a.nominal, a2.jenis_akun, a.tanggal, a.id_tkk as id, 'kas_keluar' as source from transaksi_kas_keluar a inner join akun a2 on a.id_akun = a2.id_akun) x where x.tanggal >= '" . $s . "' and x.tanggal <= '" . $e . "' ORDER BY x.tanggal ASC");

			$arr = array();

			foreach ($data as $key => $item) {
				$arr[$item->tanggal][$key] = $item;
			}

			ksort($arr, SORT_NUMERIC);
			return view('jurnal/index', ['data' => $arr, 'start' => date("d/m/Y", strtotime($s)), 'end' => date("d/m/Y", strtotime($e)), 's' => $s, 'e' => $e]);
		}
	}

	public function doPrint($source = 'download', $start, $end)
	{
		$s = date("Y-m-d", strtotime($start));
		$e = date("Y-m-d", strtotime($end));

		try {
			$data = DB::select("select x.* from (select a2.nama_akun, a.nominal, a2.jenis_akun, a.tanggal, a.id_tkm as id, 'kas_masuk' as source from transaksi_kas_masuk a inner join akun a2 on a.id_akun = a2.id_akun union select a2.nama_akun, a.nominal, a2.jenis_akun, a.tanggal, a.id_tkk as id, 'kas_keluar' as source from transaksi_kas_keluar a inner join akun a2 on a.id_akun = a2.id_akun) x where x.tanggal >= '" . $s . "' and x.tanggal <= '" . $e . "' ORDER BY x.tanggal ASC");
		} catch (\Exception $e) {
			$error = $e->getMessage();
			return redirect('jurnal')->with('alert', $error);
		}

		$arr = array();

		foreach ($data as $key => $item) {
			$arr[$item->tanggal][$key] = $item;
		}

		ksort($arr, SORT_NUMERIC);

		if ($source == 'download') {
			$PDF = PDF::loadview('jurnal/print', [
				'data' => $arr,
				's' => $s,
				'e' => $e
			]);
			return $PDF->download('Jurnal-Umum_' . date("Y-m-d_h:m:s") . '.pdf');
		} else {
			return view('jurnal/print', [
				'data' => $arr,
				's' => $s,
				'e' => $e
			]);
		}
	}

	public function showDetail($table, $id, $jenis_akun, $nama_akun)
	{
		if (!Session::get('login')) {
			return redirect('login')->with('alert', 'Kamu harus login dulu');
		} else {

			$data = null;
			if ($table == 'kas_masuk') {
				$d = TransaksiKasMasuk::where('id_tkm', $id)->first();
				if ($d) {
					$data['id'] = $d->id_tkm;
					$data['tanggal'] = $d->tanggal;
					$data['nominal'] = $d->nominal;
					$data['id_akun'] = $d->id_akun;
					$data['keterangan'] = $d->keterangan;
					$data['created_by'] = $d->created_by;
					$data['jenis_akun'] = $jenis_akun;
					$data['nama_akun'] = $nama_akun;
				}
			} else {
				$d = TransaksiKasKeluar::where('id_tkk', $id)->first();
				if ($d) {
					$data['id'] = $d->id_tkk;
					$data['tanggal'] = $d->tanggal;
					$data['nominal'] = $d->nominal;
					$data['id_akun'] = $d->id_akun;
					$data['keterangan'] = $d->keterangan;
					$data['created_by'] = $d->created_by;
					$data['jenis_akun'] = $jenis_akun;
					$data['nama_akun'] = $nama_akun;
				}
			}

			return view('jurnal/detail', ['data' => $data]);
		}
	}

	static public function rp($data)
	{
		$value = number_format($data, 2, ',', '.');
		return $value;
	}
}
