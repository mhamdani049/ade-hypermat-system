<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\PencairanDana;
use App\Akun;
use App\TransaksiKasMasuk;

use DataTables, PDF;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class KasMasukController extends Controller
{
    public function json()
    {
        $data = DB::table('pencairan_dana')
            ->select(
                'pencairan_dana.id_pencairan',
                'pencairan_dana.tanggal_pdk',
                'pencairan_dana.nominal_pdk',
                'pencairan_dana.status',
                'pencairan_dana.reason'
            )
            ->whereIn('pencairan_dana.status', [0, 1])
            ->get();

        return Datatables::of($data)->make(true);
    }

    public function showIndex()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('kasMasuk/index');
        }
    }

    public function showEdit($id)
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data = PencairanDana::where('id_pencairan', $id)->first();
            $akun = Akun::get();
            return view('kasMasuk/edit', [
                'data' => $data,
                'akun' => $akun,
            ]);
        }
    }

    public function doUpdate(Request $request)
    {
        try {
            PencairanDana::where(
                'id_pencairan',
                $request->id_pencairan
            )->update([
                'status' => $request->status,
                'reason' => $request->reason
            ]);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('kasMasuk')->with('alert', $error);
        }

        if ($request->status == 1) {
            $transaksiKasMasuk = new TransaksiKasMasuk();
            $transaksiKasMasuk['tanggal'] = $request->tanggal;
            $transaksiKasMasuk['nominal'] = $request->nominal;
            $transaksiKasMasuk['id_akun'] = $request->id_akun;
			$transaksiKasMasuk['id_pencairan'] = $request->id_pencairan;
			$transaksiKasMasuk['keterangan'] = $request->keterangan;
            $transaksiKasMasuk['created_by'] = session()->get('user')['id'];

            try {
                $transaksiKasMasuk->save();
            } catch (\Exception $e) {
                $error = $e->getMessage();
                return redirect('kasMasuk')->with('alert', $error);
            }
        }

        return redirect('kasMasuk')->with(
            'alert-success',
            'Berhasil update'
        );
	}
	
	public function showJurnalKasMasuk(Request $request) {
		if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
			$s = date('Y-m-d'). ' 00:00:00';
			$e = date('Y-m-d'). ' 23:59:59';
			$data = DB::select("select a.*, a2.nama_akun, a2.jenis_akun from ade_hypermart_system.transaksi_kas_masuk a inner join akun a2 on a.id_akun = a2.id_akun where a.tanggal >= '".$s."' and a.tanggal <= '".$e."'");
            return view('jurnalKasMasuk/index', ['datas' => $data, 'start' => date('d/m/Y'), 'end' => date('d/m/Y'), 's' => $s, 'e' => $e]);
        }
	}

	public function doSearchJurnalKasMasuk(Request $request) {
		if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
			$s = date('Y-m-d'). ' 00:00:00';
			$e = date('Y-m-d'). ' 23:59:59';

			if (isset($request->start)) 
				$s = date("Y-m-d", strtotime($request->start)). ' 00:00:00';

			if (isset($request->end)) 
				$e = date("Y-m-d", strtotime($request->end)). ' 23:59:59';

			$data = DB::select("select a.*, a2.nama_akun, a2.jenis_akun from ade_hypermart_system.transaksi_kas_masuk a inner join akun a2 on a.id_akun = a2.id_akun where a.tanggal >= '".$s."' and a.tanggal <= '".$e."'");
            return view('jurnalKasMasuk/index', ['datas' => $data, 'start' => date("d/m/Y", strtotime($s)), 'end' => date("d/m/Y", strtotime($e)), 's' => $s, 'e' => $e]);
        }
	}

	public function doPrint($source = 'download', $start, $end) {
		$s = date("Y-m-d", strtotime($start));
		$e = date("Y-m-d", strtotime($end));

		try {
            $data = DB::select("select a.*, a2.nama_akun, a2.jenis_akun from ade_hypermart_system.transaksi_kas_masuk a inner join akun a2 on a.id_akun = a2.id_akun where a.tanggal >= '".$s."' and a.tanggal <= '".$e."'");
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('jurnalKasMasuk')->with('alert', $error);
		}

		if ($source == 'download') {
			$PDF = PDF::loadview('jurnalKasMasuk/print', [
				'data' => $data, 
				's' => $s, 
				'e' => $e
			]);
			return $PDF->download('Jurnal-Kas-Masuk_'.date("Y-m-d_h:m:s").'.pdf');
		} else {
			return view('jurnalKasMasuk/print', [
				'data' => $data,
				's' => $s, 
				'e' => $e
            ]);
		}
	}

	static public function rp($data){
		$value = number_format($data,2,',','.');
		return $value;
	}

}
