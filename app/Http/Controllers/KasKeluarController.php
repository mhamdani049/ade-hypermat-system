<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\PengajuanDana;
use App\Akun;
use App\TransaksiKasKeluar;

use DataTables, PDF;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class KasKeluarController extends Controller
{
    public function json()
    {
        $data = DB::table('pengajuan_dana')
            ->select(
                'pengajuan_dana.id_pengajuan',
                'pengajuan_dana.tanggal_pjd',
                'pengajuan_dana.nominal_pjd',
                'pengajuan_dana.status',
                'pengajuan_dana.reason'
            )
			->whereIn('pengajuan_dana.status', [0, 1])
			->get();

        return Datatables::of($data)->make(true);
    }

    public function showIndex()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('kasKeluar/index');
        }
    }

    public function showEdit($id)
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data = PengajuanDana::where('id_pengajuan', $id)->first();
            $akun = Akun::get();
            return view('kasKeluar/edit', [
                'data' => $data,
                'akun' => $akun,
            ]);
        }
    }

    public function doUpdate(Request $request)
    {
        try {
            PengajuanDana::where(
                'id_pengajuan',
                $request->id_pengajuan
            )->update([
                'status' => $request->status,
                'reason' => $request->reason
            ]);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('kasKeluar')->with('alert', $error);
		}
		
		if ($request->status == 1) {
			$transaksiKasKeluar = new TransaksiKasKeluar();
			$transaksiKasKeluar['tanggal'] = $request->tanggal;
			$transaksiKasKeluar['nominal'] = $request->nominal;
			$transaksiKasKeluar['id_akun'] = $request->id_akun;
			$transaksiKasKeluar['id_pengajuan'] = $request->id_pengajuan;
			$transaksiKasKeluar['keterangan'] = $request->keterangan;
			$transaksiKasKeluar['created_by'] = session()->get('user')['id'];
			
			try {
				$transaksiKasKeluar->save();
			} catch (\Exception $e) {
				$error = $e->getMessage();
				return redirect('kasKeluar')->with('alert', $error);
			}
		}

        return redirect('kasKeluar')->with(
            'alert-success',
            'Berhasil update'
        );
	}

	public function showJurnalKasKeluar(Request $request) {
		if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
			$s = date('Y-m-d'). ' 00:00:00';
			$e = date('Y-m-d'). ' 23:59:59';
			$data = DB::select("select a.*, a2.nama_akun, a2.jenis_akun from ade_hypermart_system.transaksi_kas_keluar a inner join akun a2 on a.id_akun = a2.id_akun where a.tanggal >= '".$s."' and a.tanggal <= '".$e."'");
            return view('jurnalKasKeluar/index', ['datas' => $data, 'start' => date('d/m/Y'), 'end' => date('d/m/Y'), 's' => $s, 'e' => $e]);
        }
	}

	public function doSearchJurnalKasKeluar(Request $request) {
		if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
			$s = date('Y-m-d'). ' 00:00:00';
			$e = date('Y-m-d'). ' 23:59:59';

			if (isset($request->start)) 
				$s = date("Y-m-d", strtotime($request->start)). ' 00:00:00';

			if (isset($request->end)) 
				$e = date("Y-m-d", strtotime($request->end)). ' 23:59:59';

			$data = DB::select("select a.*, a2.nama_akun, a2.jenis_akun from ade_hypermart_system.transaksi_kas_keluar a inner join akun a2 on a.id_akun = a2.id_akun where a.tanggal >= '".$s."' and a.tanggal <= '".$e."'");
            return view('jurnalKasKeluar/index', ['datas' => $data, 'start' => date("d/m/Y", strtotime($s)), 'end' => date("d/m/Y", strtotime($e)), 's' => $s, 'e' => $e]);
        }
	}

	public function doPrint($source = 'download', $start, $end) {
		$s = date("Y-m-d", strtotime($start));
		$e = date("Y-m-d", strtotime($end));

		try {
            $data = DB::select("select a.*, a2.nama_akun, a2.jenis_akun from ade_hypermart_system.transaksi_kas_keluar a inner join akun a2 on a.id_akun = a2.id_akun where a.tanggal >= '".$s."' and a.tanggal <= '".$e."'");
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('jurnalKasKeluar')->with('alert', $error);
		}

		if ($source == 'download') {
			$PDF = PDF::loadview('jurnalKasKeluar/print', [
				'data' => $data, 
				's' => $s, 
				'e' => $e
			]);
			return $PDF->download('Jurnal-Kas-Keluar_'.date("Y-m-d_h:m:s").'.pdf');
		} else {
			return view('jurnalKasKeluar/print', [
				'data' => $data,
				's' => $s, 
				'e' => $e
            ]);
		}
		
	}

	static public function rp($data){
		$value = number_format($data,2,',','.');
		return $value;
	}
}
