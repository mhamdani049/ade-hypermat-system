<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\PencairanDana;
use App\Akun;
use App\Finance;

use DataTables, PDF;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class PencairanDanaController extends Controller
{
    public function json()
    {
        $data = DB::table('pencairan_dana')
            ->select(
                'pencairan_dana.id_pencairan',
                'pencairan_dana.tanggal_pdk',
                'pencairan_dana.nominal_pdk',
                'pencairan_dana.status',
                'pencairan_dana.reason'
            )->get();

        return Datatables::of($data)->make(true);
	}

    public function showIndex()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('pencairanDana/index');
        }
    }

    public function showAdd()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $akun = Akun::get();
            return view('pencairanDana/form', ['akun' => $akun]);
        }
    }

    public function doAdd(Request $request)
    {
		$prefix = 'PDK'.date('dmy');  
		$idg = IdGenerator::generate(['table' => 'pencairan_dana', 'field' => 'id_pencairan', 'length' => 11, 'prefix' => $prefix]);

        $pencairanDana = new PencairanDana();
        $pencairanDana['id_pencairan'] = $idg;
        $pencairanDana['tanggal_pdk'] = $request->tanggal_pdk;
        $pencairanDana['nominal_pdk'] = $request->nominal_pdk;
        $pencairanDana['id_pengguna_fe'] = session()->get('user')['id'];

        try {
            $pencairanDana->save();
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('pencairanDana')->with('alert', $error);
        }

        return redirect('pencairanDana')->with(
            'alert-success',
            'Berhasil simpan'
        );
    }

    public function showEdit($id)
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data = PencairanDana::where('id_pencairan', $id)->first();
            $akun = Akun::get();
            return view('pencairanDana/edit', [
                'data' => $data,
                'akun' => $akun,
            ]);
        }
    }

    public function doUpdate(Request $request)
    {
        try {
            PencairanDana::where(
                'id_pencairan',
                $request->id_pencairan
            )->update([
                'tanggal_pdk' => $request->tanggal_pdk,
                'nominal_pdk' => $request->nominal_pdk,
                // 'id_akun' => $request->id_akun,
            ]);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('pencairanDana')->with('alert', $error);
        }

        return redirect('pencairanDana')->with(
            'alert-success',
            'Berhasil update'
        );
    }

    public function doDelete($id)
    {
        try {
            PencairanDana::where('id_pencairan', $id)->delete();
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('pencairanDana')->with('alert', $error);
        }

        return redirect('pencairanDana')->with(
            'alert-success',
            'Berhasil hapus'
        );
	}
	
	public function doPrint($id, $source = 'download') {
		try {
            $data = PencairanDana::where('id_pencairan', $id)->first();
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('pencairanDana')->with('alert', $error);
		}

		$data['terbilang'] = self::terbilang($data->nominal_pdk);
		$data->nominal_pdk = self::rp($data->nominal_pdk);
		$data['created_by'] = 'Finance';
		$data['jabatan'] = 'Admin';
		
		if ($data->id_pengguna_fe) {
			$dataFinance = Finance::where('id_pengguna_fe', $data->id_pengguna_fe)->first();
			$data['created_by'] = $dataFinance->name;
			$data['jabatan'] = $dataFinance->jabatan;
		}

		if ($source == 'download') {
			$PDF = PDF::loadview('pencairanDana/print', ['data' => $data]);
			return $PDF->download($data->id_pencairan.'_Pencairan_'.date("Y-m-d_h:m:s").'.pdf');
		} else {
			return view('pencairanDana/print', [
                'data' => $data
            ]);
		}
		
	}

	function rp($data){
		$value = "Rp " . number_format($data,2,',','.');
		return $value;
	}

	function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = self::penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = self::penyebut($nilai/10)." puluh". self::penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . self::penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = self::penyebut($nilai/100) . " ratus" . self::penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . self::penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = self::penyebut($nilai/1000) . " ribu" . self::penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = self::penyebut($nilai/1000000) . " juta" . self::penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = self::penyebut($nilai/1000000000) . " milyar" . self::penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = self::penyebut($nilai/1000000000000) . " trilyun" . self::penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
 
	function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(self::penyebut($nilai));
		} else {
			$hasil = trim(self::penyebut($nilai));
		}     		
		return $hasil;
	}
}
