<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function showIndex()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {

			$data = DB::select("SELECT 
					(SELECT 
							COUNT(id_pencairan)
						FROM
							pencairan_dana) AS total_pencairan,
					(SELECT 
							COUNT(id_pengajuan)
						FROM
							pengajuan_dana) AS total_pengajuan,
					(SELECT 
							COUNT(id_tkm)
						FROM
							transaksi_kas_masuk) AS total_kas_masuk,
					(SELECT 
							COUNT(id_tkk)
						FROM
							transaksi_kas_keluar) AS total_kas_keluar
				FROM DUAL");

            return view('home/index', ['data' => $data[0]]);
        }
    }
}
