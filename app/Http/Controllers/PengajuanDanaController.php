<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\PengajuanDana;
use App\Akun;
use App\DepartemenLain;

use DataTables, PDF;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class PengajuanDanaController extends Controller
{
    public function json()
    {
        $data = DB::table('pengajuan_dana')
            ->select(
                'pengajuan_dana.id_pengajuan',
                'pengajuan_dana.tanggal_pjd',
                'pengajuan_dana.nominal_pjd',
                'pengajuan_dana.status',
                'pengajuan_dana.reason'
            )
            ->get();

        return Datatables::of($data)->make(true);
    }

    public function showIndex()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('pengajuanDana/index');
        }
    }

    public function showAdd()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $akun = Akun::get();
            return view('pengajuanDana/form', ['akun' => $akun]);
        }
    }

    public function doAdd(Request $request)
    {
		$prefix = 'PDK'.date('dmy');  
		$idg = IdGenerator::generate(['table' => 'pengajuan_dana','field' => 'id_pengajuan', 'length' => 11, 'prefix' => $prefix]);

        $pengajuanDana = new PengajuanDana();
        $pengajuanDana['id_pengajuan'] = $idg;
        $pengajuanDana['tanggal_pjd'] = $request->tanggal_pjd;
        $pengajuanDana['nominal_pjd'] = $request->nominal_pjd;
        $pengajuanDana['id_pengguna_dl'] = session()->get('user')['id'];

        try {
            $pengajuanDana->save();
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('pengajuanDana')->with('alert', $error);
        }

        return redirect('pengajuanDana')->with(
            'alert-success',
            'Berhasil simpan'
        );
    }

    public function showEdit($id)
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data = PengajuanDana::where('id_pengajuan', $id)->first();
            $akun = Akun::get();
            return view('pengajuanDana/edit', [
                'data' => $data,
                'akun' => $akun,
            ]);
        }
    }

    public function doUpdate(Request $request)
    {
        try {
            PengajuanDana::where(
                'id_pengajuan',
                $request->id_pengajuan
            )->update([
                'tanggal_pjd' => $request->tanggal_pjd,
                'nominal_pjd' => $request->nominal_pjd
            ]);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('pengajuanDana')->with('alert', $error);
        }

        return redirect('pengajuanDana')->with(
            'alert-success',
            'Berhasil update'
        );
    }

    public function doDelete($id)
    {
        try {
            PengajuanDana::where('id_pengajuan', $id)->delete();
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('pengajuanDana')->with('alert', $error);
        }

        return redirect('pengajuanDana')->with(
            'alert-success',
            'Berhasil hapus'
        );
	}
	
	public function doPrint($id, $source = 'download') {
		try {
            $data = PengajuanDana::where('id_pengajuan', $id)->first();
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return redirect('pengajuanDana')->with('alert', $error);
		}

		$data['terbilang'] = self::terbilang($data->nominal_pjd);
		$data->nominal_pjd = self::rp($data->nominal_pjd); // Terakhir disini
		$data['created_by'] = 'Departemen Lain';
		$data['jabatan'] = 'Admin';
		
		if ($data->id_pengguna_dl) {
			$dataDepartemenLain = DepartemenLain::where('id_pengguna_dl', $data->id_pengguna_dl)->first();
			$data['created_by'] = $dataDepartemenLain->name;
			$data['jabatan'] = $dataDepartemenLain->jabatan;
		}


		if ($source == 'download') {
			$PDF = PDF::loadview('pengajuanDana/print', ['data' => $data]);
			return $PDF->download($data->id_pengajuan.'_Pengajuan_'.date("Y-m-d_h:m:s").'.pdf');
		} else {
			return view('pengajuanDana/print', [
                'data' => $data
            ]);
		}
		
	}

	function rp($data){
		$value = "Rp " . number_format($data,2,',','.');
		return $value;
	}

	function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = self::penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = self::penyebut($nilai/10)." puluh". self::penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . self::penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = self::penyebut($nilai/100) . " ratus" . self::penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . self::penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = self::penyebut($nilai/1000) . " ribu" . self::penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = self::penyebut($nilai/1000000) . " juta" . self::penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = self::penyebut($nilai/1000000000) . " milyar" . self::penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = self::penyebut($nilai/1000000000000) . " trilyun" . self::penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
 
	function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(self::penyebut($nilai));
		} else {
			$hasil = trim(self::penyebut($nilai));
		}     		
		return $hasil;
	}

}
