<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akun extends Model
{
    protected $table = 'akun';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
        'id_akun',
		'nama_akun',
		'jenis_akun'
	];
	
	public function pencairanDana() {
        return $this->hasMany('App\PencairanDana');
    }
}
