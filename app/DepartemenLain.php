<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartemenLain extends Model
{
    protected $table = 'departemen_lain';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
        'id_pengguna_dl',
		'name',
		'password_dl',
		'id_akses',
		'jabatan'
    ];
}
