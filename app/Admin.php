<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admin';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
        'id_pengguna_adm',
		'name',
		'password_adm',
		'id_akses',
		'jabatan'
    ];
}
