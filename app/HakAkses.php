<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HakAkses extends Model
{
    protected $table = 'hak_akses';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
        'id_akses',
        'nama'
    ];
}
