<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengajuanDana extends Model
{
    protected $table = 'pengajuan_dana';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
		'id',
        'id_pengajuan',
		'tanggal_pjd',
		'nominal_pjd',
		'id_pengguna_dl',
		'status',
		'reason',
		'created_by'
	];

	public $incrementing = true;
}
