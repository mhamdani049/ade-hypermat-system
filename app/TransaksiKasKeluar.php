<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiKasKeluar extends Model
{
    protected $table = 'transaksi_kas_keluar';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
        'id_tkk',
		'tanggal',
		'nominal',
		'id_akun',
		'keterangan',
		'created_by'
	];
	
	public function akun() {
        return $this->belongsTo('App\Akun');
	}
}
