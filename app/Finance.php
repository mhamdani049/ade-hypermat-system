<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    protected $table = 'finance';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
        'id_pengguna_fe',
		'name',
		'password_fe',
		'id_akses',
		'jabatan'
    ];
}
