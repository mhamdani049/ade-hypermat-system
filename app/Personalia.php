<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personalia extends Model
{
    protected $table = 'personalia';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
        'id_pengguna',
		'name',
		'password',
		'id_akses',
		'jabatan'
    ];
}
