<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PencairanDana extends Model
{
    protected $table = 'pencairan_dana';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
		'id',
        'id_pencairan',
		'tanggal_pdk',
		'nominal_pdk',
		'id_pengguna_fe',
		'status',
		'reason',
		'created_by'
	];
	
	public $incrementing = false;
}
