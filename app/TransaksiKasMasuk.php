<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiKasMasuk extends Model
{
    protected $table = 'transaksi_kas_masuk';

    protected $dates = [
        'created_at', 
        'updated_at'
    ];

    protected $fillable = [
        'id_tkm',
		'tanggal',
		'nominal',
		'id_akun',
		'keterangan',
		'created_by'
	];
	
	public function akun() {
        return $this->belongsTo('App\Akun');
	}
}
